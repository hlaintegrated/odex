<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['copyright']	= '©2019-2020 All Rights Reserved. ODEX® is a registered trademark of MAMPU (Malaysian Administrative Modernisation and Management Planning Unit)';
$lang['login']  = 'Email';
$lang['password']  = 'Password';
$lang['ckan']  = 'CKAN';
$lang['odex']  = 'OPEN DATA EXCHANGE (ODEX)';
$lang['signin']  = 'Sign in';
$lang['rememberme']  = 'Remember Me';
$lang['signinaccount']  = 'Sign in to your account';
$lang['dashboard']  = 'Dashboard';
$lang['home']  = 'Home';
$lang['name']  = 'Name';
$lang['role']  = 'Role';
$lang['restriction']  = 'Restriction';

$lang['settings']  = 'settings';
$lang['users']  = 'users';
$lang['organisations']  = 'organisations';
$lang['organisation']  = 'organisation';
$lang['site']  = 'site';
$lang['logs']  = 'Logs';
$lang['email']  = 'Email';
$lang['total_data_sources']  = 'Total Data Sources';
$lang['api_requests']  = 'API Requests';
$lang['data_sources_types']  = 'Data Sources Types';
$lang['data_sources_organisations']  = 'Data Sources by Organisations';
$lang['data_source']  = 'Data Source';
$lang['data_source_name']  = 'Data Source Name';
$lang['data_source_description']  = 'Data Source Description';
$lang['success']  = 'success';

$lang['fail']  = 'fail';

$lang['type']  = 'type';
$lang['title']  = 'title';
$lang['status']  = 'status';
$lang['action']  = 'action';
$lang['create_data_source']  = 'Create Data Source';
$lang['update_data_source']  = 'Update Data Source';

$lang['users_management']  = 'Users Management';
$lang['create_user']  = 'Create User';
$lang['update_user']  = 'Update User';

$lang['site_configurations']  = 'Site Configurations';
$lang['create_site_config']  = 'Create Site Configuration';
$lang['update_site_config']  = 'Update Site Configuration';
$lang['config_name']  = 'Nama';
$lang['config_value']  = 'Nilai';
$lang['config_description']  = 'Description';
$lang['config_status']  = 'Configuration Status';
$lang['config_locked']  = 'Configuration Restrictions';

$lang['organisations_management']  = 'Organisation Management';
$lang['create_organisation']  = 'Create Organisation';
$lang['update_organisation']  = 'Update Organisation';
$lang['code_organisation_msg']  = 'Maxsimum 10 characters ex. MOF, MOT';
$lang['organisation_status']  = 'Organisation Status';
$lang['organisation_locked']  = 'Organisation Restrictions';

$lang['first_name']  = 'First Name';
$lang['last_name']  = 'Last Name';
$lang['organisation_name']  = 'Organisation Name';
$lang['value']  = 'value';
$lang['description']  = 'description';
$lang['code']  = 'code';


$lang['direct_api']  = 'Direct API';
$lang['sftp_file']  = 'SFTP File';
$lang['data_source_name']  = 'Data Source Name';
$lang['required']  = 'required';
$lang['required_field']  = 'Please fill in this field';
$lang['required_email']  = 'Please fill in this field';
$lang['security_header']  = 'Security Header';

$lang['search_data_files']  = 'Search Data Files';
$lang['status_locked']  = 'Status & Locked';
$lang['active']  = 'active';
$lang['locked']  = 'Locked';
$lang['unlocked']  = 'unlocked';
$lang['enable_automation']  = 'enable automation';
$lang['automation_desc']  = 'Enable this to allow ODEX fetch latest data on daily basis';
$lang['cron_job_desc']  = 'Enable Automation';
$lang['cancel']  = 'cancel';
$lang['month']  = 'month';
$lang['data_types']  = 'Data Types';

$lang['password_length']  = 'Minimum of 6 characters.';
$lang['sample_email']  = 'user@email.com';
$lang['page_title']  = 'MAMPU - OPEN DATA EXCHANGE';

$lang['resetpassword']  = 'Reset Password';
$lang['resetpasswordmsg']  = 'Please enter your Email address to received new password.';
$lang['resetpasswordmsgsuccess']  = 'Reset request submmitted, Please check your email.';
$lang['contactus']  = 'Contact Us';

$lang['submit']  = 'Submit';

$lang['apiurl']  = 'Alamat URL API';

$lang['superadministrator']  = 'Super Administrator';
$lang['administrator']  = 'Administrator';
$lang['standarduser']  = 'Standard User';

$lang['resetpasswordmsgerror']  = 'Please Enter Your Email Address';

$lang['site_config_name']  = 'Please Enter Configuration Name';
$lang['site_config_value']  = 'Please Enter Value such a API Token or Security Codes';

$lang['login_error_msg']  = 'Your Username or Password are invalid';

$lang['delete']  = 'Delete';
$lang['edit']  = 'Edit';
$lang['viewdata']  = 'View Source Data';
$lang['close']  = 'Close';

$lang['filename']  = 'File Name';
$lang['filename_desc']  = '';
$lang['filename_sample']  = 'file-name-test';

$lang['id']  = 'id';
$lang['processtype']  = 'Process Type';
$lang['message']  = 'Message';
$lang['date']  = 'Date';
$lang['search']  = 'Search';

$lang['updated']  = 'Updated';

$lang['proceed']  = 'Proceed';
$lang['clusters']  = 'Clusters';
$lang['license']  = 'License';

$lang['inactive']  = 'Inactive';

$lang['data_sources_ckan']  = 'Total Processed Data Sources at CKAN';

$lang['datatables_entries']  = 'Showing <b>_START_ to _END_</b> of _TOTAL_ entries';

$lang['datatables_empty']  = 'No data available in table';

$lang['datatables_zero']  = 'No matching records found';
$lang['datatables_filter']  = '- filtered from _MAX_ records';

$lang['datatables_infoempty']  = '';

$lang['invalid_email']  =  "Please include an ‘@’ in the email address. missing an ‘@’";

$lang['file_exist']  = 'Requested file does not exist';

$lang['success_data']  = '<strong>Success: </strong> Data Added / Updated';
$lang['success_delete']  = '<strong>Success: </strong> Data Removed';
$lang['fail_data']  = '<strong>Error: </strong> Data Added / Updated Failed Please Check Logs';
$lang['data_locked']  = '<strong>Error: </strong> Update Operation abort, data locked by Administrator';
$lang['data_active']  = 'strong>Error: </strong> This Organisation has Active Data Sources ';
$lang['data_abort']  = '<strong>Error: </strong> Operation abort, Please Check Logs';


