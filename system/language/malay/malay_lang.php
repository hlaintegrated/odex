<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['copyright']	= '©2019-2020 Hakcipta Terpelihara MAMPU (Unit Pemodenan Tadbiran dan Perancangan Pengurusan Malaysia)';
$lang['login']  = 'Daftar Masuk';
$lang['password']  = 'Kata Laluan';
$lang['ckan']  = 'CKAN';
$lang['odex']  = 'OPEN DATA EXCHANGE (ODEX)';
$lang['signin']  = 'Log Masuk';
$lang['rememberme']  = 'Ingat Saya';
$lang['signinaccount']  = 'Log Masuk Akaun';
$lang['dashboard']  = 'Dashboard';
$lang['home']  = 'Utama';
$lang['name']  = 'Nama';
$lang['role']  = 'Peranan';
$lang['restriction']  = 'Sekatan';

$lang['settings']  = 'Tetapan';
$lang['users']  = 'Pengguna';
$lang['organisations']  = 'Kementerian / Agensi';
$lang['organisation']  = 'Kementerian / Agensi';
$lang['site']  = 'Laman';
$lang['logs']  = 'Catatan';
$lang['email']  = 'E-mel';
$lang['total_data_sources']  = 'Jumlah Sumber Data';
$lang['api_requests']  = 'Permintaan API';
$lang['data_sources_types']  = 'Jenis Sumber Data';
$lang['data_sources_organisations']  = 'Sumber Data mengikut Kementerian / Agensi';
$lang['data_source']  = 'Sumber Data';
$lang['data_source_name']  = 'Tajuk Sumber Data';
$lang['data_source_description']  = 'Keterangan Sumber Data';
$lang['success']  = 'lulus';

$lang['fail']  = 'Gagal';
$lang['disable']  = 'Tidak Aktif';

$lang['type']  = 'Format Fail';
$lang['title']  = 'Tajuk Set Data';
$lang['status']  = 'status';
$lang['action']  = 'Tindakan';
$lang['create_data_source']  = 'Cipta Sumber Data';
$lang['update_data_source']  = 'Kemaskini Sumber Data';

$lang['users_management']  = 'Pengurusan Pengguna';
$lang['create_user']  = 'Cipta Pengguna';
$lang['update_user']  = 'Kemaskini Pengguna';

$lang['site_configurations']  = 'Konfigurasi Laman';
$lang['create_site_config']  = 'Cipta Konfigurasi Laman';
$lang['update_site_config']  = 'Kemaskini Konfigurasi Laman';
$lang['config_name']  = 'Name';
$lang['config_value']  = 'Value';
$lang['config_description']  = 'Keterangan';
$lang['config_status']  = 'Status Konfigurasi';
$lang['config_locked']  = 'Sekatan Konfigurasi';

$lang['organisations_management']  = 'Pengurusan Organisasi';
$lang['create_organisation']  = 'Cipta Organisasi';
$lang['update_organisation']  = 'Kemaskini Organisasi';
$lang['code_organisation_msg']  = 'Maksimum 10 aksara cth. MOF, MOT';
$lang['organisation_status']  = 'Status Organisasi';
$lang['organisation_locked']  = 'Sekatan Organisasi';

$lang['first_name']  = 'Nama Pertama';
$lang['last_name']  = 'Nama Akhir';
$lang['organisation_name']  = 'Nama Kementerian / Agensi';
$lang['value']  = 'nilai';
$lang['description']  = 'Keterangan';
$lang['code']  = 'Kod';

$lang['clusters']  = 'Kluster';
$lang['license']  = 'Lesen';

$lang['direct_api']  = 'API secara Terus';
$lang['sftp_file']  = 'Fail SFTP';
$lang['data_source_name']  = 'Nama Sumber Data';
$lang['required']  = 'mandatori';
$lang['required_field']  = 'Sila isi medan ini';
$lang['required_email']  = 'Please fill in this field';
$lang['security_header']  = 'Token Keselamatan';

$lang['search_data_files']  = 'Carian Fail Data';
$lang['status_locked']  = 'Status & Sekatan';
$lang['active']  = 'Aktif';
$lang['locked']  = 'Disekat';
$lang['unlocked']  = 'Buka Sekatan';
$lang['enable_automation']  = 'Aktifkan Automasi';
$lang['automation_desc']  = 'Mengaktifkan ini untuk membenarkan ODEX mengambil data terkini setiap hari.';
$lang['cron_job_desc']  = '#####';
$lang['cancel']  = 'Batal';
$lang['month']  = 'bulan';
$lang['data_types']  = 'Jenis Data';

$lang['password_length']  = 'Minimum of 6 characters.';
$lang['sample_email']  = 'user@email.com';
$lang['page_title']  = 'MAMPU - OPEN DATA EXCHANGE (ODEX)';

$lang['resetpassword']  = 'Tetapkan semula kata laluan';
$lang['resetpasswordmsg']  = 'Sila masukan E-mel untuk menerima kata laluan baru.';
$lang['resetpasswordmsgsuccess']  = 'Permohonan kata laluan baru telah di hantar, sila semak e-mel anda.';
$lang['contactus']  = 'Hubungi Kami';

$lang['submit']  = 'Hantar';

$lang['apiurl']  = 'Alamat URL API';

$lang['superadministrator']  = 'Pentadbir Sistem';
$lang['administrator']  = 'Pentadbir';
$lang['standarduser']  = 'Penguna Biasa';

$lang['resetpasswordmsgerror']  = 'Sila Masukan Alamat E-mel Anda';

$lang['site_config_name']  = 'Sila Masukan Nama Konfigurasi';
$lang['site_config_value']  = 'Sila Masukan Nilai seperti Token untuk API atau Kod Keselamatan';

$lang['login_error_msg']  = 'Nama Pengguna atau Kata Laluan Anda Tidak Sah';


$lang['delete']  = 'Padam';
$lang['edit']  = 'Edit';
$lang['viewdata']  = 'Lihat Data';
$lang['close']  = 'Tutup';

$lang['filename']  = 'Nama Fail';
$lang['filename_desc']  = '';
$lang['filename_sample']  = 'contoh-nama-fail';

$lang['id']  = 'id';
$lang['processtype']  = 'Jenis Proses';
$lang['message']  = 'Mesej';
$lang['date']  = 'Tarikh';
$lang['search']  = 'Carian';

$lang['updated']  = 'Kemaskini';

$lang['proceed']  = 'Teruskan';

$lang['inactive']  = 'Tidak Aktif';

$lang['data_sources_ckan']  = 'Jumlah Sumber Data Proses di CKAN';

$lang['datatables_entries']  = 'Keputusan <b>_START_ sehingga _END_</b> dari _TOTAL_ Data';

$lang['datatables_empty']  = 'Data tidak ada dalam jadual';

$lang['datatables_zero']  = 'Tiada rekod yang sepadan';
$lang['datatables_filter']  = '- ditapis dari _MAX_ rekod ';

$lang['datatables_infoempty']  = '';

$lang['invalid_email']  =  'Sila masukkan simbol @ dalam alamat e-mel. tiada simbol @';

$lang['file_exist']  = 'Fail yang diminta tidak wujud';

$lang['success_data']  = '<strong>Berjaya: </strong> Data  Ditambah/Dikemaskini.';
$lang['success_delete']  = '<strong>Berjaya: </strong> Data dipadamkan.';
$lang['fail_data']  = '<strong>Kesilapan: </strong> Data tidak berjaya Dtambah/Dikemaskini, Sila semak Catatan.';
$lang['data_locked']  = '<strong>Kesilapan: </strong> Operasi tidak berjaya, data disekat oleh Pentadbir Sistem.';
$lang['data_active']  = 'strong>Kesilapan: </strong> Organisasi ini mempunyai sumber data yang masih aktif.';
$lang['data_abort']  = '<strong>Kesilapan: </strong> Operasi tidak berjaya, Sila semak Catatan.';







