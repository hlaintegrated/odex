<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct() {
    parent::__construct();
	  $this->load->database();
    
   }


   public function get_datasourcesByOrg($oid)
   {

     $query = $this->db->query("SELECT * FROM datasources WHERE oid = '".$oid."'"); 
     return $query->result_array();
    
   }

   public function get_datasources()
   {

     $query = $this->db->query("SELECT a.file_name as file_name,b.name as orgname,a.name as dtname,a.type as type,a.status as status,a.locked as locked,a.id as id,a.oid as oid FROM datasources a, organisations b WHERE b.id = a.oid"); 
     return $query->result_array();
    
   }

   public function get_datasource($id)
   {

     $query = $this->db->query("SELECT * FROM datasources WHERE id =".$id); 
     return $query->result_array();
    
   }
   ///graphs

   public function get_total_datasources_byorg($oid=NULL)
   {
    if(empty($oid)){
     $query = $this->db->query("SELECT b.code as code,count(a.id) as total FROM datasources a,organisations b WHERE a.oid = b.id GROUP BY a.oid");
    } else {
     $query = $this->db->query("SELECT count(id) as totaldata,SUM(if(ckan_resource_id != '', 1, 0)) AS ckan FROM `datasources` WHERE oid = ".$oid);
    }
     return $query->result_array();
    
   }

   public function get_total_datasources_bymonth($oid=NULL)
   {
     if(empty($oid)){
     $query = $this->db->query("SELECT  DATE_FORMAT(`updated`,'%M %Y') AS monthyear,SUM(if(status = 1, 1, 0)) AS active, SUM(if(status = 0, 1, 0)) AS inactive FROM `datasources` Group By DATE_FORMAT(`updated`,'%M %Y')"); 
     } else {
      $query = $this->db->query("SELECT  DATE_FORMAT(`updated`,'%M %Y') AS monthyear,SUM(if(status = 1, 1, 0)) AS active, SUM(if(status = 0, 1, 0)) AS inactive FROM `datasources` WHERE oid = ".$oid." Group By DATE_FORMAT(`updated`,'%M %Y')"); 
     }
     return $query->result_array();
    
   }

   public function get_total_datasources_bytype($oid=NULL)
   {
    if(empty($oid)){
     $query = $this->db->query("SELECT type ,count(id) as total FROM datasources GROUP BY type"); 
    } else {
    $query = $this->db->query("SELECT type ,count(id) as total FROM datasources WHERE oid = ".$oid." GROUP BY type");
    }
     return $query->result_array();
    
   }

   // end graph

   public function get_organisation($id)
   {

     $query = $this->db->query("SELECT * FROM organisations WHERE id = '".$id."'"); 
     return $query->result_array();
    
   }

   public function get_organisations()
   {

     $query = $this->db->query("SELECT * FROM organisations"); 
     return $query->result_array();
    
   }

   public function get_site_configs()
   {

     $query = $this->db->query("SELECT * FROM site"); 
     return $query->result_array();
    
   }

   public function get_logs($id=NULL)
   {
     if(empty($id)){
      $query = $this->db->query("SELECT * FROM logs"); 
     } else {
      $query = $this->db->query("SELECT * FROM logs WHERE datasource_id =".$id); 
     }
     
     return $query->result_array();
    
   }

   public function get_site_config($id)
   {

     $query = $this->db->query("SELECT * FROM site WHERE id =".$id); 
     return $query->result_array();
    
   }

   public function get_users($uid=null,$oid=null)
   {

     $query = $this->db->query("SELECT a.id as id,a.locked as locked,a.oid as oid,
     a.fname as fname,a.lname as lname,a.email as email,b.name as organisation,a.status as status 
     FROM users a, organisations b 
     WHERE a.oid = b.id "); 
     return $query->result_array();
    
   }

   public function get_user($uid=null)
   {

     $query = $this->db->query("SELECT * FROM users WHERE id = ".$uid); 
     return $query->result_array();
    
   }

   public function get_locked($table,$id)
   {

     $query = $this->db->query("SELECT id FROM ".$table." WHERE id = ".$id." AND locked = 1"); 
     return $query->result_array();
    
   }


   public function create($table,$data)
    {
         
    $this->db->replace($table,$data);
    $insertId = $this->db->insert_id();
    return $insertId;
    
    }

   public function update($table,$data,$id)
    {
         
    $this->db->where('id', $id);
    $this->db->update($table, $data);
    
     }

   public function delete($table,$id)
    {

    $this->db->where('id', $id);
    $this->db->delete($table);

    }
    
     

}


