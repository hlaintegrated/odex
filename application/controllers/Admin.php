<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  function __construct() {
    parent::__construct();
    if(isset($_SESSION["language"])){
      $this->lang->load($_SESSION["language"],$_SESSION["language"]);
    } else {
      $_SESSION["language"] = "malay"; 
      $this->lang->load('malay', 'malay');
    }

  }
  
	public function index()
	{

    $this->load->view('login');
  }
  
  public function lang()
	{
    $langval = $this->uri->segment(3);
    unset($_SESSION["language"]);
    if($langval == "my") {  
      $_SESSION["language"] = "malay"; 
    } else {
      $_SESSION["language"] = "english";
    }
    redirect("admin");
	}

	public function dashboard()
	{
    $data = [];
    $data["alert"] = $this->uri->segment(3);
    $data['section'] = 'dashboard';
    $data['menus'] = $this->admin_model->get_organisations();
    $data['datasources'] =  $this->admin_model->get_datasources();
    $data['datasources_bymonths'] =  $this->admin_model->get_total_datasources_bymonth();
    $data['datasources_byorg'] =  $this->admin_model->get_total_datasources_byorg();
    $data['datasources_bytype'] =  $this->admin_model->get_total_datasources_bytype();
    $data['get_sftp_files'] = scandir(getcwd().'/raw_data/',1);
    $this->template->load('template', 'dashboard', $data);

  }

  public function organization()
	{
    $data = [];
    $data['section'] = 'organisation';
		$this->template->load('template', 'dashboard', $data);

  }

  public function logs()
	{
    $data = [];
    $data['section'] = 'logs';
    $data['menus'] = $this->admin_model->get_organisations();
    $id = $this->uri->segment(3);
    $data['logs'] =  $this->admin_model->get_logs($id);
		$this->template->load('template', 'dashboard', $data);

  }

  public function viewdata()
	{
    $data = [];
    $data['section'] = 'viewdata';
    $data['menus'] = $this->admin_model->get_organisations();
    $name = $this->uri->segment(3);
    $filename = 'data/'.$name.'.csv';
    $data["csv_datas"] = [];
    if (file_exists($filename)) {

    if (($h = fopen("{$filename}", "r")) !== FALSE) {
         while (($csv = fgetcsv($h, 1000, ",")) !== FALSE) 
           {
              $data["csv_datas"][] = $csv;		
           }
       }
       fclose($h); 
    }    
    $data["name"] = $name;
		$this->template->load('template', 'dashboard', $data);

  }
  


  public function datasources()
	{
    $data = [];
    $data['section'] = 'datasources';
    $oid = $this->uri->segment(3);
    $data['edit'] = $did = $this->uri->segment(4);
    $data['datasources_bytype'] =  $this->admin_model->get_total_datasources_bytype();
    $data['organisation'] = $this->admin_model->get_organisation($oid);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['datasources'] =  $this->admin_model->get_datasourcesByOrg($oid);

    $data['datasources_bymonths'] =  $this->admin_model->get_total_datasources_bymonth($oid);
    $data['datasources_byorg'] =  $this->admin_model->get_total_datasources_byorg($oid);
    $data['datasources_bytype'] =  $this->admin_model->get_total_datasources_bytype($oid);

    if(!empty($did)){
      $data['editdatasource'] = $this->admin_model->get_datasource($did); }
		$this->template->load('template', 'dashboard', $data);

  }



  public function users()
	{
    $data = [];
    $data['section'] = 'users';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['users'] = $this->admin_model->get_users();
    if(!empty($id)){
    $data['edituser'] = $this->admin_model->get_user($id); }
		$this->template->load('template', 'dashboard', $data);
	
  }
  public function organisations()
	{
    $data = [];
    $data['section'] = 'organisations';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $data['organisations'] = $this->admin_model->get_organisations();
    if(!empty($id)){
    $data['editorganisation'] = $this->admin_model->get_organisation($id);}
		$this->template->load('template', 'dashboard', $data);

  }
  public function site()
	{
    $data = [];
    $data['section'] = 'site';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['sitesconfig'] = $this->admin_model->get_site_configs();
    if(!empty($id)){
      $data['editsite'] = $this->admin_model->get_site_config($id);}
    
		$this->template->load('template', 'dashboard', $data);

  }

  public function getresponseid()
  {

    $result = '{"help": "https://demo.ckan.org/api/3/action/help_show?name=resource_create", "success": true, "result": {"cache_last_updated": null, "cache_url": null, "mimetype_inner": null, "hash": "", "description": "descriptions", "format": "CSV", "url": "http://api.data.gov.my/data/bnm.csv", "created": "2020-04-30T15:16:05.924479", "state": "active", "package_id": "40883dd9-7264-471f-921f-87d77042d7cd", "last_modified": null, "mimetype": null, "url_type": null, "position": 1, "revision_id": "df0e6573-71bc-439d-ac9a-1119dcb8e061", "size": null, "datastore_active": false, "id": "780e5a08-bf77-4070-952e-d964bf322d09", "resource_type": null, "name": "202004300316 BNM Base-Rate BKKBMYKL"}}';
    $result = '{"help": "http://150.242.183.95/data/api/3/action/help_show?name=datastore_create", "success": false, "error": {"resource_id": ["Not found: Resource"], "__type": "Validation Error"}}';
    $result = '{"help": "https://catalog.sarawak.gov.my/api/3/action/help_show?name=datastore_search", "success": true, "result": {"include_total": true, "resource_id": "23582381-a8df-4c84-857d-0b96ec0c7194", "fields": [{"type": "int", "id": "_id"}, {"type": "text", "id": "Division / District"}, {"type": "numeric", "id": "Area (sq. km)"}, {"type": "numeric", "id": "Percentage Distribution"}], "records_format": "objects", "records": [{"_id":1,"Division / District":"Kuching","Area (sq. km)":1498,"Percentage Distribution":1.2036962635596626380873885864275507628917694091796875},{"_id":2,"Division / District":"Bau","Area (sq. km)":884,"Percentage Distribution":0.7103254319003615702143861199147067964076995849609375},{"_id":3,"Division / District":"Lundu","Area (sq. km)":1812,"Percentage Distribution":1.4560064282844515926029771435423754155635833740234375},{"_id":4,"Division / District":"KUCHING DIVISION","Area (sq. km)":4195,"Percentage Distribution":3.37002812374447557886014692485332489013671875},{"_id":5,"Division / District":"Samarahan","Area (sq. km)":407,"Percentage Distribution":0.327038971474487738522185509282280690968036651611328125}], "limit": 5, "_links": {"start": "/api/3/action/datastore_search?limit=5&resource_id=23582381-a8df-4c84-857d-0b96ec0c7194", "next": "/api/3/action/datastore_search?offset=5&limit=5&resource_id=23582381-a8df-4c84-857d-0b96ec0c7194"}, "total": 86}}';
    $results = json_decode($result, true);
    

    echo "<pre>";
    print_r($results);
    echo "</pre>";
    $resultso = $results["result"]["records"];
    $processDate = date("Ymdhis");
    $fp = fopen(getcwd().'/data/'.$processDate.'.csv', 'w');

    fputcsv($fp,array_keys($resultso[0]));
    foreach ($resultso as $result) {
       
          fputcsv($fp, $result);
        
        
      }
    fclose($fp);

    die;
    //echo $result->result->package_id;
    //echo $result->result->id;
    echo $result->success;
    if(empty($result->success)){
      $data["msg"] = json_encode($result);
      $data["created"] = date('Y-m-d H:i');
      $data["type"] = 1;
      $table = "logs";
      $this->admin_model->create($table,$data);
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("data_abort").'</div>');
      redirect('admin/dashboard');
    }

  }




  public function crud()
	{
    
    $processDate = date("Ymdhis");
    $table = $_POST["table"];
    if(isset($_POST["id"])){  $id = $_POST["id"]; }

    //if(isset($_POST["status"])){ $_POST["status"] = 1; } else { $_POST["status"] = 0; };
    if(isset($_POST["password"]) && strlen($_POST["password"]) != 32 && !preg_match('/^[a-f0-9]{32}$/',$_POST["password"])){ $_POST["password"] = md5($_POST["password"]); }
   
    if(isset($_POST["oid"])){ $oid = $_POST["oid"]; }
    
    if($table == 'datasources' && empty($id)){

          if($_POST["type"] == 'json'){ 
          $result = $this->getDataApi($_POST["url"],$processDate);

          if($result === false){
            $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert">
            <button aria-label="" class="close" data-dismiss="alert"></button>Struktur Data API Tidak Dikenali</div>');
            redirect('admin/dashboard');
            exit;
          }
        }
    
          if($_POST["type"] == 'csv'){ 
                  $dataurl = trim($_POST["file_name"]);
                  //$source = getcwd().'/raw_data/'.$dataurl.'.csv';
                  //$destination = getcwd().'/data/'.$processDate.'.csv';
                  //copy($source,$destination);
                  //$dataurl = $processDate;
 
                  } else { 

                  $_POST["file_name"] = $dataurl = $processDate; 
               }
      $packagename = strtolower(str_replace(" ","-",trim($_POST["name"])));
      $packagedesc = $_POST["description"];
      $packageclusters = $_POST["clusters"];
      $packagename_raw = $_POST["name"];
      $org = $this->admin_model->get_organisation($_POST["oid"]);

      $createpackage = $this->ckan_package_create($packagename,$org[0]["code"],$packagedesc,$packageclusters);
      $createpackage = json_decode($createpackage,true);
      $success = $createpackage["success"];
      
      if(empty($success)){
        $errormsg = json_encode($createpackage["error"]);
        $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
        <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("data_abort").$errormsg.'</div>');
        redirect('admin/dashboard');
        exit;

      }
      $result = $this->ckan_resource_create($packagename,$dataurl,$packagename_raw,$packagedesc);
      $result = json_decode($result);
      $_POST["ckan_package_id"] = $result->result->package_id;
      $_POST["ckan_resource_id"] = $resource_id = $result->result->id;
      $_POST["ckan_url"] = "http://www2.data.gov.my/data/dataset/".$packagename."/resource/".$resource_id;

      if(empty($result->success)){
        $data["msg"] = json_encode($result);
        $data["created"] = date('Y-m-d H:i');
        $data["type"] = 1;
        $table = "logs";
        $this->admin_model->create($table,$data);
        $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
        <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("data_abort").'</div>');
        redirect('admin/dashboard');
        exit;
      }
      

    }

    unset($_POST["table"]);

    if(!empty($id) && $_SESSION["role"] != 1){
    $locked = $this->admin_model->get_locked($table,$id);

    if(count($locked) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button> '.$this->lang->line("error_data").' </div>');
      if(!empty($oid)){
        redirect('admin/'.$table.'/'.$oid); 
        } else {
           redirect('admin/'.$table); 
         }
       } 
    }
    if(empty($id)){ 
           unset($_POST["id"]);
           $_POST["created"] = date('Y-m-d H:i');
           $_POST["updated"] = date('Y-m-d H:i');
           $this->admin_model->create($table,$_POST);
        } else {
          $_POST["updated"] = date('Y-m-d H:i');
           $this->admin_model->update($table,$_POST,$id);
        }
  
    

    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">
    <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("success_data").'</div>');
    if($table == 'datasources'){
      redirect('admin/'.$table.'/'.$oid); 
    } else {
    redirect('admin/'.$table); 
    }

  }

  public function delete()
	{
    $table = $this->uri->segment(3);
    $id = $this->uri->segment(4);
    $oid = $this->uri->segment(5);
    
    $locked = $this->admin_model->get_locked($table,$id);

    if(count($locked) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("data_locked").'</div>');
      if(!empty($oid)){
        redirect('admin/'.$table.'/'.$oid); 
        } else {
           redirect('admin/'.$table); 
         }
    } 
    
    if($table == 'organisations'){
    $datasources = $this->admin_model->get_datasourcesByOrg($id);
    if(count($datasources) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("data_active").'</div>');
      redirect('admin/'.$table); 
        } 
     }
     
    if($table == 'datasources'){
    $dtsource = $this->admin_model->get_datasource($id); 
    $this->ckan_resource_delete($dtsource[0]["ckan_resource_id"]);
    }

    $this->admin_model->delete($table,$id);

    

    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">
    <button aria-label="" class="close" data-dismiss="alert"></button>'.$this->lang->line("success_delete").'</div>');
    if($table == 'datasources'){
                 redirect('admin/'.$table.'/'.$oid.'/'); 
                 } else {
                redirect('admin/'.$table.'/'); 
            }
  
  }


	public function getDataApi($url=NULL,$processDate=NULL)
	{
		$curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => trim($url),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
               "Accept: application/vnd.BNM.API.v1+json",
               "Access-Control-Allow-Origin: *"
            ),
        ));

       $response = curl_exec($curl);

       curl_close($curl);

        $results = json_decode($response, true);

        if(strpos($url,'bnm') !== false){
          $resultso = $results["data"];
        } else{
          $resultso = $results["result"]["records"];
        }
        
        //print_r($resultso[0]);
        if (array_key_exists(0,$resultso)){
       
             $fp = fopen(getcwd().'/data/'.$processDate.'.csv', 'w');
       
            
             fputcsv($fp,array_keys($resultso[0])); 
     
             foreach ($resultso as $result) {

                 if (fputcsv($fp, $result) === false) {
                      return false;
                }
 
             }
            fclose($fp);
        } else {
          return false;
        } 
	}



	public function processapi()
	{
		$data = [];
    $data['getjson'] = $response = $this->getData($this->uri->segment(3));
    $data['packageid'] = $packageid = $this->getpackageid();
		$data['ckancreateresource'] = $this->ckan_resource_create($packageid);
    $data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
    
    $path = getcwd();

    $fp = fopen($path.'/data/results.json', 'w');
    fwrite($fp, json_encode($response));
    fclose($fp);
    die;
		$this->template->load('template', 'processapi', $data);
	}


	public function processfile()
	{
    $data = [];
    $data['getjson'] = $this->processcsv();
    $data['packageid'] = $packageid = $this->getpackageid();
    $data['ckancreateresource'] = $this->ckan_resource_create($packageid);
		$data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
		$this->template->load('template', 'processfile', $data);
	}

	public function processcsv()
	{

		 $file = getcwd()."/data/test.csv";
         $csv= file_get_contents($file);
         $array = array_map("str_getcsv", explode("\n", $csv));
		 $json = json_encode($array, JSON_PRETTY_PRINT);
		 
         return $json;
  }
  
  public function getpackageid()
  {
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://150.242.183.95/data/api/3/action/package_list",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
    ));
    
    $response = curl_exec($curl);
    curl_close($curl);
    $packageid = json_decode($response);
    //echo '<pre>';
    //print_r($packageid);
    //echo '</pre>';
    $pid = (rand(1,1000));
    return $packageid->result[$pid];

  }

  public function ckan_package_create($packagename,$orgname,$description,$clusters)
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
         $data = json_encode(array(
         'name' => $packagename,
         'groups' => array(["name"=> $clusters]),
         'owner_org'=> $orgname,
         'author' => 'ODEX',
         'author_email' => 'odex@data.gov.my',
         'maintainer' => 'ODEX',
         'maintainer_email' => 'odex@data.gov.my',
         'notes' => $description,
         'license_id' => "cc-by",
         'title' => $packagename
      
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://www2.data.gov.my/data/api/3/action/package_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 6a7b52e5-28a5-42d9-a977-9a2e3bc26d22"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
      
        return $response;
	}


	public function ckan_resource_create($packagename,$dataurl,$packagename_raw,$description)
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
         $data = json_encode(array(
		     'url' => 'http://odex.data.gov.my/data/'.$dataurl.'.csv',
         'name' => $packagename_raw,
         'description'=> $description,
         'format'=>'csv',
         'package_id' => $packagename
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://www2.data.gov.my/data/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 6a7b52e5-28a5-42d9-a977-9a2e3bc26d22"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
        
  }

  public function ckan_resource_delete($resource_id)
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
         $data = json_encode(array(
		     'resource_id' => $resource_id,
         'force' => 'true'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/resource_delete",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 6a7b52e5-28a5-42d9-a977-9a2e3bc26d22"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
       
  }
  
	

	public function ckan_datastore_create($packageid=NULL)
	{
    $testname = date("Ymdhi");   
    $curl = curl_init();
         $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
         'url' => 'https://api.bnm.gov.my/public/base-rate/BKKBMYKL',
         'name' => $testname.' datastore',
         'description'=> 'test desc',
         'force'=> 'True', 
         'package_id' => 'test',
         'resource_id' => '04fef84c-deb7-4177-b202-45177c9b260b'
         ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/datastore_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }


  public function sendemail()
  {
     $this->load->library('email');
     $this->load->library('encrypt');
     $useremail = $this->uri->segment(3);
     $config = array(
            'protocol' => 'smtp', 
            'smtp_host' => 'smtp.googlemail.com', 
            'smtp_port' => 25, 
            'smtp_user' => 'hlaintegrated@gmail.com', 
            'smtp_pass' => '@Empatbelas14', 
            'mailtype' => 'html', 
            'charset' => 'iso-8859-1');
    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
 
    //Email content
    $htmlContent = '<h1>Permohonan Set Data</h1>';
    $htmlContent .= '<p>Permohonan Set Data untuk "Jumlah Kenderaan di Sarawak di Luluskan" </p>';
 
    $this->email->to($useremail);
    $this->email->from('noreply@dev.data.gov.my','Data Terbuka MAMPU');
    $this->email->subject('Permohonan Set Data');
    $this->email->message($htmlContent);
 
     //Send email
     $this->email->send();
     echo "email sent";

}




}
