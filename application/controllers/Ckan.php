<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}

	public function dashboard()
	{
    $data = [];
    $data["alert"] = $this->uri->segment(3);
    $data['section'] = 'dashboard';
    $data['menus'] = $this->admin_model->get_organisations();
    $data['datasources'] =  $this->admin_model->get_datasources();
    $this->template->load('template', 'dashboard', $data);

  }

  public function organization()
	{
    $data = [];
    $data['section'] = 'organisation';
		$this->template->load('template', 'dashboard', $data);

  }
  


  public function datasources()
	{
    $data = [];
    $data['section'] = 'datasources';
    $oid = $this->uri->segment(3);
    $data['edit'] = $did = $this->uri->segment(4);
    $data['organisation'] = $this->admin_model->get_organisation($oid);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['datasources'] =  $this->admin_model->get_datasourcesByOrg($oid);
    if(!empty($did)){
      $data['editdatasource'] = $this->admin_model->get_datasource($did); }
		$this->template->load('template', 'dashboard', $data);

  }



  public function users()
	{
    $data = [];
    $data['section'] = 'users';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['users'] = $this->admin_model->get_users();
    if(!empty($id)){
    $data['edituser'] = $this->admin_model->get_user($id); }
		$this->template->load('template', 'dashboard', $data);
	
  }
  public function organisations()
	{
    $data = [];
    $data['section'] = 'organisations';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $data['organisations'] = $this->admin_model->get_organisations();
    if(!empty($id)){
    $data['editorganisation'] = $this->admin_model->get_organisation($id);}
		$this->template->load('template', 'dashboard', $data);

  }
  public function site()
	{
    $data = [];
    $data['section'] = 'site';
    $data['edit'] = $id = $this->uri->segment(3);
    $data['menus'] = $this->admin_model->get_organisations();
    $data['sitesconfig'] = $this->admin_model->get_site_configs();
    if(!empty($id)){
      $data['editsite'] = $this->admin_model->get_site_config($id);}
    
		$this->template->load('template', 'dashboard', $data);

  }


  public function crud()
	{
    
  
    $table = $_POST["table"];
    if(isset($_POST["id"])){ $id = $_POST["id"];}
    //if(isset($_POST["status"])){ $_POST["status"] = 1; } else { $_POST["status"] = 0; };
    if(strlen($_POST["password"]) != 32 && !preg_match('/^[a-f0-9]{32}$/',$_POST["password"])){ $_POST["password"] = md5($_POST["password"]); }
  
    if(isset($_POST["oid"])){ $oid = $_POST["oid"]; }
    unset($_POST["table"]);

    if(!empty($id) && $_SESSION["role"] != 1){
    $locked = $this->admin_model->get_locked($table,$id);

    if(count($locked) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button> <strong>Error: </strong> Update Operation abort, data locked by Administrator </div>');
      if(!empty($oid)){
        redirect('admin/'.$table.'/'.$oid); 
        } else {
           redirect('admin/'.$table); 
         }
       } 
    }
    if(empty($id)){ unset($_POST["id"]); }
    $this->admin_model->create($table,$_POST);

    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">
    <button aria-label="" class="close" data-dismiss="alert"></button> <strong>Success: </strong> Data Added / Updated </div>');
    if($table == 'datasources'){
      redirect('admin/'.$table.'/'.$oid); 
    } else {
    redirect('admin/'.$table); 
    }

  }

  public function delete()
	{
    $table = $this->uri->segment(3);
    $id = $this->uri->segment(4);
    $oid = $this->uri->segment(5);
    
    $locked = $this->admin_model->get_locked($table,$id);

    if(count($locked) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button> <strong>Error: </strong> Delete Operation abort, data locked by Administrator </div>');
      if(!empty($oid)){
        redirect('admin/'.$table.'/'.$oid); 
        } else {
           redirect('admin/'.$table); 
         }
    } 
    
    if($table == 'organisations'){
    $datasources = $this->admin_model->get_datasourcesByOrg($id);
    if(count($datasources) > 0){
      $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert">
      <button aria-label="" class="close" data-dismiss="alert"></button> <strong>Error: </strong> This Organisation has Active Data Sources </div>');
      redirect('admin/'.$table); 
        } 
     }

    $this->admin_model->delete($table,$id);

    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert">
    <button aria-label="" class="close" data-dismiss="alert"></button> <strong>Success: </strong> Data Removed </div>');
            if(!empty($oid)){
                 redirect('admin/'.$table.'/'.$oid); 
                 } else {
                redirect('admin/'.$table); 
            }
  
  }


	public function getData($data)
	{
		$curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.bnm.gov.my/public/base-rate/".$data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
               "Accept: application/vnd.BNM.API.v1+json"
            ),
        ));

       $response = curl_exec($curl);

       curl_close($curl);
       return $response;
	}



	public function processapi()
	{
		$data = [];
    $data['getjson'] = $response = $this->getData($this->uri->segment(3));
    $data['packageid'] = $packageid = $this->getpackageid();
		$data['ckancreateresource'] = $this->ckan_resource_create($packageid);
    $data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
    
    $path = getcwd();

    $fp = fopen($path.'/data/results.json', 'w');
    fwrite($fp, json_encode($response));
    fclose($fp);
    die;
		$this->template->load('template', 'processapi', $data);
	}


	public function processfile()
	{
    $data = [];
    $data['getjson'] = $this->processcsv();
    $data['packageid'] = $packageid = $this->getpackageid();
    $data['ckancreateresource'] = $this->ckan_resource_create($packageid);
		$data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
		$this->template->load('template', 'processfile', $data);
	}

	public function processcsv()
	{

		 $file = getcwd()."/data/test.csv";
         $csv= file_get_contents($file);
         $array = array_map("str_getcsv", explode("\n", $csv));
		 $json = json_encode($array, JSON_PRETTY_PRINT);
		 
         return $json;
  }
  
  public function getpackageid()
  {
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://150.242.183.95/data/api/3/action/package_list",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
    ));
    
    $response = curl_exec($curl);
    curl_close($curl);
    $packageid = json_decode($response);
    //echo '<pre>';
    //print_r($packageid);
    //echo '</pre>';
    $pid = (rand(1,1000));
    return $packageid->result[$pid];

  }

  public function ckan_package_create()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
     $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'name' => 'local-banks-currency-rates',
         'notes' => 'dataset',
         'owner_org'=> 'bank-negara'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/package_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
	}


	public function ckan_resource_create()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/bnm.json',
         'name' => $testname.' currency dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'local-banks-currency-rates'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/1");
  }
  
  public function ckan_resource_create_demo()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/test.json',
         'name' => $testname.' currency dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  
  public function ckan_resource_create_sarawak()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/sarawak.json',
         'name' => $testname.' sarawak dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  

  public function ckan_resource_create_bnm()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/bnm.json',
         'name' => $testname.' BNM dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  
  public function ckan_resource_create_dosm()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/test.csv',
         'name' => $testname.' DOSM CSV file dataset',
         'description'=> 'test desc',
         'format'=>'csv', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
	}
	

	public function ckan_datastore_create($packageid)
	{
    $testname = date("Ymdhi");   
    $curl = curl_init();
         $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
         'url' => 'https://api.bnm.gov.my/public/base-rate/BKKBMYKL',
         'name' => $testname.' datastore',
         'description'=> 'test desc',
         'force'=> 'True', 
         'package_id' => $packageid,
         'resource_id' => '04fef84c-deb7-4177-b202-45177c9b260b'
         ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/datastore_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


  public function sendemail()
  {
     $this->load->library('email');
     //$this->load->library('encrypt');
     $useremail = $this->uri->segment(3);
     $config = array(
            'protocol' => 'smtp', 
            'smtp_host' => 'ssl://smtp.gmail.com', 
            'smtp_port' => 465, 
            'smtp_user' => 'hlaintegrated@gmail.com', 
            'smtp_pass' => '@Empatbelas14', 
            'mailtype' => 'html', 
            'charset' => 'iso-8859-1');
    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
 
    //Email content
    $htmlContent = '<h1>Permohonan Set Data</h1>';
    $htmlContent .= '<p>Permohonan Set Data untuk "Jumlah Kenderaan di Sarawak di Luluskan" </p>';
 
    $this->email->to($useremail);
    $this->email->from('noreply@dev.data.gov.my','Data Terbuka MAMPU');
    $this->email->subject('Permohonan Set Data');
    $this->email->message($htmlContent);
 
     //Send email
     $this->email->send();
     echo "email sent";

}




}
