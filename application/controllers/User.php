<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(isset($_SESSION["language"])){
		  $this->lang->load($_SESSION["language"],$_SESSION["language"]);
		} else {
		  $this->lang->load('malay', 'malay');
		}
	
	  }


	public function index()
	{
		redirect("main");
	}

	public function login()
        {
		
		// get form input
		$email = $this->input->post("email");
        $password = $this->input->post("password");

		// form validation
		$this->form_validation->set_rules("email", "Email");
		$this->form_validation->set_rules("password", "Password", "md5");



		if ($this->form_validation->run() == FALSE)
        {
			// validation fail
			$this->load->view('dashboard');
		}
		else
		{   
			$user =  $this->user_model->get_user($email,md5($password));
		
            if(count($user) > 0) {

                       $sess_data = array('login' => TRUE,'email' => $user[0]['email'],'oid' => $user[0]['oid'],'role' => $user[0]['role']);
				       $this->session->set_userdata($sess_data);
				       redirect("admin/dashboard/");

					   } else {

                       $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">'.$this->lang->line("login_error_msg").'</div>');
					   redirect('admin'); 
					}

		  }


		}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	}

	public function create()
	{
		//to do
	}

	public function update()
	{
		//to do
	}

	public function delete()
	{
		//to do
	}

	public function get_user()
	{
		//to do
	}

	public function get_users()
	{
		//to do
	}
	  

  


}
