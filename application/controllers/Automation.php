<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Automation extends CI_Controller {

	public function index()
	{
		redirect("admin");
	}


	public function getData($data)
	{
    
    //model get datasources
    $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.bnm.gov.my/public/base-rate/".$data,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
               "Accept: application/vnd.BNM.API.v1+json"
            ),
        ));

       $response = curl_exec($curl);

       curl_close($curl);
       //convert json to csv
       // save csv file
       // update field file

       return $response;
	}



	public function processapi()
	{
		$data = [];
    $data['getjson'] = $response = $this->getData($this->uri->segment(3));
    $data['packageid'] = $packageid = $this->getpackageid();
		$data['ckancreateresource'] = $this->ckan_resource_create($packageid);
    $data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
    
    $path = getcwd();

    $fp = fopen($path.'/data/results.json', 'w');
    fwrite($fp, json_encode($response));
    fclose($fp);
    die;
		$this->template->load('template', 'processapi', $data);
	}


	public function processfile()
	{
    $data = [];
    $data['getjson'] = $this->processcsv();
    $data['packageid'] = $packageid = $this->getpackageid();
    $data['ckancreateresource'] = $this->ckan_resource_create($packageid);
		$data['ckandatastorecreate'] = $this->ckan_datastore_create($packageid);
		$this->template->load('template', 'processfile', $data);
	}

	public function processcsv()
	{

		 $file = getcwd()."/data/test.csv";
         $csv= file_get_contents($file);
         $array = array_map("str_getcsv", explode("\n", $csv));
		 $json = json_encode($array, JSON_PRETTY_PRINT);
		 
         return $json;
  }
  
  public function getpackageid()
  {
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://150.242.183.95/data/api/3/action/package_list",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
    ));
    
    $response = curl_exec($curl);
    curl_close($curl);
    $packageid = json_decode($response);
    //echo '<pre>';
    //print_r($packageid);
    //echo '</pre>';
    $pid = (rand(1,1000));
    return $packageid->result[$pid];

  }

  public function ckan_package_create()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
     $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'name' => 'local-banks-currency-rates',
         'notes' => 'dataset',
         'owner_org'=> 'bank-negara'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/package_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
	}


	public function ckan_resource_create()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/bnm.json',
         'name' => $testname.' currency dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'local-banks-currency-rates'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/1");
  }
  
  public function ckan_resource_create_demo()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/test.json',
         'name' => $testname.' currency dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  
  public function ckan_resource_create_sarawak()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/sarawak.json',
         'name' => $testname.' sarawak dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  

  public function ckan_resource_create_bnm()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/bnm.json',
         'name' => $testname.' BNM dataset',
         'description'=> 'test desc',
         'format'=>'json', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
  }
  
  public function ckan_resource_create_dosm()
	{
     
    $testname = date("Ymdhi");
    $curl = curl_init();
		 $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
		     'url' => 'http://api.data.gov.my/data/test.csv',
         'name' => $testname.' DOSM CSV file dataset',
         'description'=> 'test desc',
         'format'=>'csv', 
         'package_id' => 'bank-interest-rates-2020'
		 ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://demo.ckan.org/api/3/action/resource_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
       CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        redirect("main/dashboard/2");
	}
	

	public function ckan_datastore_create($packageid)
	{
    $testname = date("Ymdhi");   
    $curl = curl_init();
         $file = getcwd()."/data/test.csv";
         $data = json_encode(array(
         'url' => 'https://api.bnm.gov.my/public/base-rate/BKKBMYKL',
         'name' => $testname.' datastore',
         'description'=> 'test desc',
         'force'=> 'True', 
         'package_id' => $packageid,
         'resource_id' => '04fef84c-deb7-4177-b202-45177c9b260b'
         ));

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://150.242.183.95/data/api/3/action/datastore_create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
                  "Authorization: 35dee1e8-32f6-46cd-aadb-ce370c2d883b"
                ),
          ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


  public function sendemail()
  {
     $this->load->library('email');
     //$this->load->library('encrypt');
     $useremail = $this->uri->segment(3);
     $config = array(
            'protocol' => 'smtp', 
            'smtp_host' => 'ssl://smtp.gmail.com', 
            'smtp_port' => 465, 
            'smtp_user' => 'hlaintegrated@gmail.com', 
            'smtp_pass' => '@Empatbelas14', 
            'mailtype' => 'html', 
            'charset' => 'iso-8859-1');
    $this->email->initialize($config);
    $this->email->set_mailtype("html");
    $this->email->set_newline("\r\n");
 
    //Email content
    $htmlContent = '<h1>Permohonan Set Data</h1>';
    $htmlContent .= '<p>Permohonan Set Data untuk "Jumlah Kenderaan di Sarawak di Luluskan" </p>';
 
    $this->email->to($useremail);
    $this->email->from('noreply@dev.data.gov.my','Data Terbuka MAMPU');
    $this->email->subject('Permohonan Set Data');
    $this->email->message($htmlContent);
 
     //Send email
     $this->email->send();
     echo "email sent";

}




}
