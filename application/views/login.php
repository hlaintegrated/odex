<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $this->lang->line("page_title"); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="<?= base_url();?>assets/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url();?>assets/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url();?>assets/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url();?>assets/ico/152.png">
    <link rel="icon" type="image/x-icon" href="<?= base_url();?>assets/ico/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Malaysian Administrative Modernisation and Management Planning Unit" name="description" />
    <meta content="Ace" name="author" />
    <link href="<?= base_url();?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?= base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link class="main-stylesheet" href="<?= base_url();?>assets/css/themes/light.css" rel="stylesheet" type="text/css" />
    <!-- Please remove the file below for production: Contains demo classes -->
    <link class="main-stylesheet" href="<?= base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <style>
    .login-wrapper {
    background: #FFF;
    /*background: url("assets/img/odexbg.jpg");*/
    width: 100%;
    height: 100%;
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: bottom;
    }
    </style>
    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header menu-pin">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic" id="deco">
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20" >
         
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 p-r-50 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
        <?php if (isset($_SESSION["language"]) && $_SESSION["language"] === 'english'): ?>
        <img class="img-fluid" src="assets/img/logoenglish.png" />
        <?php else: ?>
          <img class="img-fluid" src="assets/img/logomalay.png" /> 
          <?php endif; ?>


        <img class="mx-auto d-block" width="150px" src="assets/img/dtsalogo.png" />
          <h4 class="text-center"><?php echo $this->lang->line("odex"); ?></h4>
          <p class="mw-80 m-t-5"><?php echo $this->lang->line("signinaccount"); ?></p>
          <!-- START Login Form -->
          <?php echo $this->session->flashdata('msg'); ?>
        <form action="<?php echo base_url();?>user/login" method="post">
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label><?php echo $this->lang->line("email"); ?></label>
              <div class="controls">
                <input type="text" name="email" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"  placeholder="<?php echo $this->lang->line("sample_email"); ?>" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label><?php echo $this->lang->line("password"); ?></label>
              <div class="controls">
                <input type="password" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);"  class="form-control" name="password" placeholder="<?php echo $this->lang->line("password"); ?>" required>
              </div>
            </div>
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-6 no-padding sm-p-l-10">
                <div class="checkbox ">
                  <input type="checkbox" value="1" id="checkbox1">
                  <label for="checkbox1"><?php echo $this->lang->line("rememberme"); ?></label>
                </div>
              </div>

              <div class="col-md-6 d-flex align-items-center justify-content-end">
                <button aria-label="" class="btn btn-primary btn-lg m-t-10" type="submit"><?php echo $this->lang->line("signin"); ?></button>
              </div>
            </div>
            
            <!-- END Form Control-->
          </form>
          
          <!--END Login Form-->

          <div class="m-b-5 m-t-30">
              <a href="#" class="normal" data-target="#testsourcemodal" data-toggle="modal"><?php echo $this->lang->line("resetpassword"); ?></a>
            </div>
            <div>
              <a href="#" class="normal" data-target="#contactus" data-toggle="modal"><?php echo $this->lang->line("contactus"); ?></a>
            </div>
          
          <div class="pull-bottom sm-pull-bottom">
          <div class="col-sm-10 no-padding m-t-10 text-center"><a href="<?= base_url();?>admin/lang/my"> Bahasa Melayu </a> | <a href="<?= base_url();?>admin/lang/en"> English </a></div>
            <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
              <div class="col-sm-10 no-padding m-t-10">
                <p class="text-center normal hint-text" style="font-size: 10px">
                <?php echo $this->lang->line("copyright"); ?> (15-6-2020-1)
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>

    <!-- MODAL STICK UP SMALL ALERT -->
<div class="modal fade stick-up" id="testsourcemodal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                    </button>
                    <h5><?php echo $this->lang->line("resetpassword"); ?></h5>
                  </div>
                  <div class="modal-body">
                    <div id="resetpasswordmodal" style="overflow:hidden">
                    <input type="text" id="emailreset" name="email" placeholder="Email" class="form-control" required>
                    <p id="resetpasswordmsgerror" style="display:none"><?php echo $this->lang->line("resetpasswordmsgerror"); ?></p>
                    <p id="resetpasswordmsgsuccess"><?php echo $this->lang->line("resetpasswordmsg"); ?></p>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <button aria-label="" type="button" id="resetpasswordbtn" class="btn btn-primary pull-left "><?php echo $this->lang->line("submit"); ?></button>
                  
                  <button aria-label="" type="button" id="resetpasswordbtnclose" class="btn btn-primary pull-left inline sendbtn" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP SMALL ALERT -->

          <!-- MODAL STICK UP SMALL ALERT -->
<div class="modal fade stick-up" id="contactus" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                    </button>
                    <h5><?php echo $this->lang->line("contactus"); ?></h5>
                  </div>
                  <div class="modal-body">
<b>UNIT PEMODENAN TADBIRAN
DAN PERANCANGAN PENGURUSAN MALAYSIA</b>
<br>Aras 6, Setia Perdana 2,
<br>Kompleks Setia Perdana,
<br>Pusat Pentadbiran Kerajaan Persekutuan
<br>62502 Putrajaya Malaysia
<br>T 603 8000 8000   
<br>F 603 8888 3721
<br>E dataterbuka[at]mampu.gov.my
                  </div>
                  <div class="modal-footer">
            
                    <button aria-label="" type="button" class="btn btn-primary pull-left inline" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                  
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP SMALL ALERT -->
    
    <!-- BEGIN VENDOR JS -->
    <script src="<?= base_url();?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <!--  A polyfill for browsers that don't support ligatures: remove liga.js if not needed-->
    <script src="<?= base_url();?>assets/plugins/liga.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/classie/classie.js"></script>
    <script src="<?= base_url();?>assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/js/three.r95.min.js"></script>
    <script src="<?= base_url();?>assets/js/vanta.net.min.js"></script>
    <!-- END VENDOR JS -->
    <script src="<?= base_url();?>assets/js/pages.min.js"></script>
    <script>
    $(function()
    {
      $('#form-login').validate()
    })
    </script>

<script>
VANTA.NET({
  el: "#deco",
  mouseControls: true,
  touchControls: true,
  minHeight: 200.00,
  minWidth: 200.00,
  scale: 1.00,
  scaleMobile: 1.00,
  color: 0x6c85d7,
  backgroundColor: 0x393939
})
</script>

<script>

$("#resetpasswordbtn").click(function(){
   var emailreset = $("#emailreset").val();
   if (emailreset == ""){
    $("#resetpasswordmsgerror").show();
    $("#resetpasswordmsgsuccess").hide();
   } else {
    $(".sendbtn").hide();
    $("#resetpasswordmodal").text(" <?php echo $this->lang->line("resetpasswordmsgsuccess"); ?>");
    
   }
});

</script>

<script>
function InvalidMsg(textbox) {
    if (textbox.value === '') {
        textbox.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>');
    } else if (textbox.validity.typeMismatch){
        textbox.setCustomValidity('<?php echo $this->lang->line("invalid_email"); ?>');
    } else {
       textbox.setCustomValidity('');
    }

    return true;
}
</script>


  </body>
</html>