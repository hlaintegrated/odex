<?php if ($section === 'dashboard'): ?>

  <div class="page-content-wrapper">

        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START JUMBOTRON -->
          <div data-pages="parallax">
            <div class="container-fluid p-l-25 p-r-25 sm-p-l-0 sm-p-r-0">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb sm-p-b-5">
                  <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("home"); ?></a></li>
                  <li class="breadcrumb-item active"><?php echo $this->lang->line("dashboard"); ?></li>
                </ol>
              </div>
            </div>
   
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid p-l-25 p-r-25 p-t-0 p-b-25 sm-padding-10">
          
          <?php echo $this->session->flashdata('msg'); ?>
            <!-- START ROW -->
            <?php if (count($datasources) > 0): ?>
            <div class="row">
            
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                
                <!-- START ITEM -->
                <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("total_data_sources"); ?><i class="pg-icon">circle_fill</i></h5>
                 
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="curve_chart"></div>
                  <div class="card-footer clearfix"></div>
                </div>
                <!-- END ITEM -->
          
                
                
              </div>
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                
                 <!-- START ITEM -->
                 <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("data_sources_types"); ?><i class="pg-icon">circle_fill</i></h5>
                   
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="piechart"></div>
                  <div class="card-footer clearfix"></div>
                  </div>
                <!-- END ITEM -->
                
              </div>
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                <!-- START ITEM -->
                <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("data_sources_organisations"); ?><i class="pg-icon">circle_fill</i></h5>
                   
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="columnchart_values" style="width:200;height: 300px;"></div>
                
                  <div class="card-footer clearfix"></div>
                
                </div>
                <!-- END ITEM -->
                
              </div>
              <?php endif; ?>

              <div class="col-lg-12 m-b-10 d-flex">
                
                 <!-- START card -->
            <div class="card card-transparent bg-white">
              <div class="card-header ">
                <div class="card-title bold">
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                   
                  </div>
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                  
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>

                <div class="pull-right">
                  <div class="col-xs-12">
                <button aria-label="" class="btn btn-default pull-right" data-target="#modalSlideLeft" data-toggle="modal"><i class="pg-icon">add</i><?php echo $this->lang->line("create_data_source"); ?></button>
               
                </div>
                </div>
                 


                <div class="clearfix"></div>
              </div>
              
              <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block lang" >
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("organisation"); ?></th>
                      <th><?php echo $this->lang->line("type"); ?></th>
                      <th><?php echo $this->lang->line("title"); ?></th>
                      <th><?php echo $this->lang->line("status"); ?></th>
                      <th><?php echo $this->lang->line("action"); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                  
                  <?php foreach ($datasources as $datasource):?>
                    <tr>
                      <td class="v-align-middle semi-bold">
                        <?php echo $datasource['orgname']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $datasource['type']; ?>
                  </td>

                      <td class="v-align-middle">
                      <?php echo $datasource['dtname']; ?>
                      </td>
                      <td class="v-align-middle">

                      <i class="pg-icon"><?php if($datasource['status'] == 1){ echo "tick";} else { echo "disable"; } ?> </i>
                      
                           
                      </td>
                      <td class="v-align-middle">
                      <button class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("locked"); ?>"><i class="pg-icon"><?php if($datasource['locked'] == 1){ echo "lock";} else { echo "unlock"; } ?></i></button>
                      <a href="<?php echo base_url().'admin/datasources/'.$datasource['oid'].'/'.$datasource['id'];?>" class="btn btn-default m-b-10" type="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("edit"); ?>"><i class="pg-icon">edit</i></a>
              
                      <?php if (isset($datasource['file_name'])): ?>
                      <span data-toggle="modal" data-target="#viewdata">
                      <a href="<?php echo base_url().'admin/viewdata/'.$datasource['file_name']; ?>" type="link" type="link" class="btn btn-default m-b-10" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("viewdata"); ?>"><i class="pg-icon">eye</i></a>
                      </span>
                      <?php endif; ?>

                        
                      <span data-toggle="modal" data-target="#delete">
                      <button class="btn btn-default m-b-10 delete" id="<?php echo $datasource['id'].'-datasources-'.$datasource['oid'].'-'.$datasource['dtname']; ?>" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("delete"); ?>" ><i class="pg-icon">trash</i></buton>
                      </span>
                          </td>
                    </tr>

                    <?php endforeach;?>

                    
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
            </div>
            <!-- END ROW -->
            
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
       
      
      </div>

      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Prasarana", "API", { role: "style" } ],
        <?php 
        foreach ($datasources_byorg as $datasource_byorg){
           echo '["'.$datasource_byorg["code"].'",'.$datasource_byorg["total"].',"#1abc9c"],';
          
        }
        ?>
       
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "",
        width: 400,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['<?php echo $this->lang->line("month"); ?>', '<?php echo $this->lang->line("active"); ?>', '<?php echo $this->lang->line("inactive"); ?>'],
          <?php 
        foreach ($datasources_bymonths as $datasource_bymonth){
           echo '["'.$datasource_bymonth["monthyear"].'",'.$datasource_bymonth["active"].','.$datasource_bymonth["inactive"].'],';
          
        }
        ?>
        ]);

        var options = {
          title: '',
          width: 400,
          height: 300,
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Totals'],
          <?php 
        foreach ($datasources_bytype as $datasource_bytype){
           echo '["'.$datasource_bytype["type"].'",'.$datasource_bytype["total"].'],';
          
        }
        ?>
    
    
        ]);

        var options = {
          title: '',
          width: 400,
          height: 300
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

     

<?php endif; ?>

<?php if ($section === 'datasources'): ?>

<div class="page-content-wrapper">

        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START JUMBOTRON -->
          <div data-pages="parallax">
            <div class="container-fluid p-l-25 p-r-25 sm-p-l-0 sm-p-r-0">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb sm-p-b-5">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $organisation[0]['name']; ?></li>
                </ol>
              </div>
            </div>
           
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid p-l-25 p-r-25 p-t-0 p-b-25 sm-padding-10">
          
          <?php echo $this->session->flashdata('msg'); ?>
            <!-- START ROW -->
            <?php if (count($datasources) > 0): ?>
            <div class="row">
            
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                
                <!-- START ITEM -->
                <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("total_data_sources"); ?><i class="pg-icon">circle_fill</i></h5>
                   
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="curve_chart"></div>
                  <div class="card-footer clearfix"></div>
                </div>
                <!-- END ITEM -->
          
                
                
              </div>
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                
                 <!-- START ITEM -->
                 <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("data_sources_types"); ?><i class="pg-icon">circle_fill</i></h5>
                    
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="piechart"></div>
                  <div class="card-footer clearfix"></div>
                  </div>
                <!-- END ITEM -->
                
              </div>
              <div class="col-lg-4 col-sm-6  d-flex flex-column">
                <!-- START ITEM -->
                <div class="card social-card share  full-width m-b-10 no-border" data-social="item" style="overflow:hidden">
                  <div class="card-header clearfix">
                    <h5 class="text-success pull-left fs-12 d-flex align-items-center"><?php echo $this->lang->line("data_sources_ckan"); ?><i class="pg-icon">circle_fill</i></h5>
                  
                    <div class="clearfix"></div>
                  </div>
                  
                  <div id="columnchart_values" style="width:200;height: 300px;"></div>
                
                  <div class="card-footer clearfix"></div>
                
                </div>
                <!-- END ITEM -->
                
              </div>
              <?php endif; ?>
              <div class="col-lg-12 m-b-10 d-flex">
                
                 <!-- START card -->
            <div class="card card-transparent bg-white">
              <div class="card-header ">
                <div class="card-title bold">
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                   
                  </div>
                </div>

                <div class="pull-left">
                  <div class="col-xs-12">
                  
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>

                <div class="pull-right">
                  <div class="col-xs-12">
                <button aria-label="" class="btn btn-default pull-right" data-target="#modalSlideLeft" data-toggle="modal"><i class="pg-icon">add</i> <?php echo $this->lang->line("create_data_source"); ?></button>
               
                </div>
                </div>

                
                <div class="clearfix"></div>
              </div>
              
              <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block lang">
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("name"); ?></th>
                      <th><?php echo $this->lang->line("type"); ?></th>
                      <th><?php echo $this->lang->line("ckan"); ?></th>
                      <th><?php echo $this->lang->line("status"); ?></th>
                      <th><?php echo $this->lang->line("action"); ?></th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php foreach ($datasources as $datasource):?>
                    <tr>
                      <td class="v-align-middle semi-bold">
                        <i class="pg-icon v-align-middle text-success">circle_fill</i><?php echo $datasource['name']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $datasource['type']; ?>
                  </td>

                      <td class="v-align-middle">
                     
                      <?php if ($datasource['ckan_url'] == NULL): ?>
                      <a href="<?php echo base_url().'admin/logs/'.$datasource['id']; ?>" aria-label="" class="btn btn-default m-b-10 text-danger" type="link"><i class="pg-icon">alert</i>CKAN</a>
                      <?php else: ?>
                        <a href="<?php echo $datasource['ckan_url']; ?>" aria-label="" class="btn btn-default m-b-10 text-success" type="button"><i class="pg-icon">globe</i>CKAN</a>
                      
                      <?php endif; ?>
                      
                    </td>
                      <td class="v-align-middle">
                      
                      <i class="pg-icon"><?php if($datasource['status'] == 1){ echo "tick";} else { echo "disable"; } ?> </i>
                    
                      </td>
  
                      <td class="v-align-middle">
                      <button class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("locked"); ?>" ><i class="pg-icon"><?php if($datasource['locked'] == 1){ echo "lock";} else { echo "unlock"; } ?></i></button>
                      <a href="<?php echo base_url().'admin/datasources/'.$datasource['oid'].'/'.$datasource['id'];?>" class="btn  btn-default m-b-10" type="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("edit"); ?>"><i class="pg-icon">edit</i></a>
                      
                      
                      <?php if (isset($datasource['file_name'])): ?>
                      <span data-toggle="modal" data-target="#viewdata">
                      <a href="<?php echo base_url().'admin/viewdata/'.$datasource['file_name']; ?>" type="link" type="link" class="btn btn-default m-b-10" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("viewdata"); ?>"><i class="pg-icon">eye</i></a>
                      </span>
                      <?php endif; ?>

                      <span data-toggle="modal" data-target="#delete">
                      <button class="btn btn-default m-b-10 delete" id="<?php echo $datasource['id'].'-datasources-'.$datasource['oid'].'-'.$datasource['name']; ?>" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("delete"); ?>" ><i class="pg-icon">trash</i></buton>
                      </span>
                      
                      <?php if (isset($datasource['updated'])): ?>
                      <button class="btn btn-default m-b-10" type="button" disabled><i class="pg-icon">calendar</i>&nbsp;&nbsp;<?php if(isset($datasource['updated'])){ echo $datasource['updated']; } ?></button>
                      <?php endif; ?>
                      </td>

                    </tr>

                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
            </div>
            <!-- END ROW -->
            
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
       
      
      </div>

      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["<?php echo $this->lang->line("total_data_sources"); ?>", " ", { role: "style" } ],
        ["Jumlah Sumber Data",<?php echo intval($datasources_byorg[0]["totaldata"]); ?>,"color: #1e90ff"],
        ["CKAN",<?php echo intval($datasources_byorg[0]["ckan"]); ?>, "color: #2ed573"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "",
        width: 400,
        height: 300,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['<?php echo $this->lang->line("month"); ?>', '<?php echo $this->lang->line("active"); ?>', '<?php echo $this->lang->line("inactive"); ?>'],
          <?php 
        foreach ($datasources_bymonths as $datasource_bymonth){
           echo '["'.$datasource_bymonth["monthyear"].'",'.$datasource_bymonth["active"].','.$datasource_bymonth["inactive"].'],';
          
        }
        ?>
        ]);

        var options = {
          title: '',
          width: 500,
          height: 300,
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Type', 'Totals'],
          <?php 
        foreach ($datasources_bytype as $datasource_bytype){
           echo '["'.$datasource_bytype["type"].'",'.$datasource_bytype["total"].'],';
          
        } ?>
        ]);

        var options = {
          title: '',
          width: 400,
          height: 300
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
 

<?php endif; ?>

<?php if ($section === 'users'): ?>
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("home"); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $this->lang->line("users_management"); ?></li>
            </ol>
            <!-- END BREADCRUMB -->
           
            <div class="row">
            <?php echo $this->session->flashdata('msg'); ?>
              <div class="col-xl-12 col-lg-12 bg-white">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-left" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                <button aria-label="" class="btn btn-default btn-lg pull-right" data-target="#modalSlideLeft" data-toggle="modal"><i class="pg-icon">add</i><?php echo $this->lang->line("create_user"); ?></button>
                
               
                </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover demo-table-dynamic table-responsive-block lang" >
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("name"); ?></th>
                      <th><?php echo $this->lang->line("email"); ?></th>
                      <th><?php echo $this->lang->line("organisation"); ?></th>
                      <th><?php echo $this->lang->line("action"); ?></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($users as $user):?>
                    <tr>
                    <td class="v-align-middle">
                      <?php echo $user['fname'].' '.$user['lname']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $user['email']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $user['organisation']; ?>
                      </td>
                      <td class="v-align-middle">
                      <button aria-label="" class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("status"); ?>"><i class="pg-icon"><?php if($user['status'] == 1){ echo "tick"; } else { echo "disable"; } ?> </i></button>
                      <button class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("locked"); ?>"><i class="pg-icon"><?php if($user['locked'] == 1){ echo "lock";} else { echo "unlock"; } ?></i></button>
                      <a href="<?php echo base_url().'admin/users/'.$user['id'];?>" class="btn btn-default m-b-10" type="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("edit"); ?>"><i class="pg-icon">edit</i></a>
                      
                      <span data-toggle="modal" data-target="#delete">
                      <button class="btn btn-default m-b-10 delete" id="<?php echo $user['id'].'-users-'.$user['oid'].'-'.$user['fname']; ?>" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("delete"); ?>" ><i class="pg-icon">trash</i></buton>
                      </span>
                    
                      </td>
                    
                     
                    </tr>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
              
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->
  
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

      <?php endif; ?> 

      <?php if ($section === 'organisations'): ?>
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("home"); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $this->lang->line("organisations_management"); ?></li>
            </ol>
            <!-- END BREADCRUMB -->
            <div class="row">
            <?php echo $this->session->flashdata('msg'); ?>
              <div class="col-xl-12 col-lg-12 bg-white">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-left" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                <button aria-label="" class="btn btn-default pull-right" data-target="#modalSlideLeft" data-toggle="modal"><i class="pg-icon">add</i><?php echo $this->lang->line("create_organisation"); ?></button>
               
                </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover demo-table-dynamic table-responsive-block lang" >
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("name"); ?></th>
                      <th><?php echo $this->lang->line("email"); ?></th>
                      <th><?php echo $this->lang->line("code"); ?></th>
                      <th><?php echo $this->lang->line("action"); ?></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($organisations as $organisation):?>
                    <tr>
                    <td class="v-align-middle">
                      <?php echo $organisation['name']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $organisation['email']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $organisation['code']; ?>
                      </td>
                      <td class="v-align-middle">
                      <button aria-label="" class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("status"); ?>"><i class="pg-icon"><?php if($organisation['status'] == 1){ echo "tick"; } else { echo "disable"; } ?> </i></button>
                      <button class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("locked"); ?>"><i class="pg-icon"><?php if($organisation['locked'] == 1){ echo "lock";} else { echo "unlock"; } ?></i></button>
                      <a href="<?php echo base_url().'admin/organisations/'.$organisation['id'];?>" class="btn btn-default m-b-10" type="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("edit"); ?>"><i class="pg-icon">edit</i></a>
                      
                      <span data-toggle="modal" data-target="#delete">
                      <button class="btn btn-default m-b-10 delete" id="<?php echo $organisation['id'].'-organisations-'.$organisation['id'].'-'.$organisation['name']; ?>" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("delete"); ?>" ><i class="pg-icon">trash</i></buton>
                      </span>
                      
                          </td>
                     
                    </tr>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
              
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->

      </div>
      <!-- END PAGE CONTENT WRAPPER -->

      <?php endif; ?> 

      <?php if ($section === 'site'): ?>
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("home"); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $this->lang->line("site_configurations"); ?></li>
            </ol>
            <!-- END BREADCRUMB -->
            <div class="row">
            <?php echo $this->session->flashdata('msg'); ?>
              <div class="col-xl-12 col-lg-12 bg-white">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-left" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                <button aria-label="" class="btn btn-default pull-right" data-target="#modalSlideLeft" data-toggle="modal"><i class="pg-icon">add</i><?php echo $this->lang->line("create_site_config"); ?></button>
               
                </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover demo-table-dynamic table-responsive-block lang">
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("name"); ?></th>
                      <th><?php echo $this->lang->line("value"); ?></th>
                      <th><?php echo $this->lang->line("description"); ?></th>
                      <th><?php echo $this->lang->line("action"); ?></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($sitesconfig as $siteconfig):?>
                    <tr>
                    <td class="v-align-middle">
                      <?php echo $siteconfig['name']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $siteconfig['value']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $siteconfig['description']; ?>
                      </td>

                      <td class="v-align-middle">
                      <button aria-label="" class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("status"); ?>"><i class="pg-icon"><?php if($siteconfig['status'] == 1){ echo "tick";} else { echo "disable"; } ?> </i></button>
                      <button class="btn btn-default m-b-10" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("locked"); ?>"><i class="pg-icon"><?php if($siteconfig['locked'] == 1){ echo "lock";} else { echo "unlock"; } ?></i></button>
                      <a href="<?php echo base_url().'admin/site/'.$siteconfig['id'];?>" class="btn btn-default m-b-10" type="link" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("edit"); ?>"><i class="pg-icon">edit</i></a>
                      
                      
                      <span data-toggle="modal" data-target="#delete">
                      <button class="btn btn-default m-b-10 delete" id="<?php echo $siteconfig['id'].'-site-sites-'.$siteconfig['name']; ?>" type="button" data-toggle="tooltip" data-placement="bottom" title="<?php echo $this->lang->line("delete"); ?>" ><i class="pg-icon">trash</i></buton>
                      </span>
                   
                          </td>
                     
                    </tr>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
              
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->
        
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

      <?php endif; ?> 

      <?php if ($section === 'logs'): ?>
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("home"); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $this->lang->line("logs"); ?></li>
            </ol>
            <!-- END BREADCRUMB -->
           
            <div class="row">

              <div class="col-xl-12 col-lg-12 bg-white">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-left" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover demo-table-dynamic table-responsive-block lang">
                  <thead>
                    <tr>
                      <th><?php echo $this->lang->line("id"); ?></th>
                      <th><?php echo $this->lang->line("processtype"); ?></th>
                      <th><?php echo $this->lang->line("message"); ?></th>
                      <th><?php echo $this->lang->line("date"); ?></th>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($logs as $log):?>
                    <tr>
                    <td class="v-align-middle">
                      <?php echo $log['datasource_id']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $log['type']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $log['msg']; ?>
                      </td>
                      <td class="v-align-middle">
                      <?php echo $log['created']; ?>
                      </td>
                    
                     
                    </tr>
                    <?php endforeach;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
              
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->
  
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

      <?php endif; ?> 


      <?php if ($section === 'viewdata'): ?>
<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
             
              <li class="breadcrumb-item"><a href="#"><?php echo $this->lang->line("viewdata"); ?></a></li>
              <li class="breadcrumb-item active"><?php echo $name; ?></li>
            </ol>
            <!-- END BREADCRUMB -->
           
            <div class="row">

              <div class="col-xl-12 col-lg-12 bg-white">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-left">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-left" placeholder="<?php echo $this->lang->line("search"); ?>">
                  </div>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div class="card-body" style="overflow:hidden">
              <?php if (count($csv_datas) > 0): ?>
                <table class="table table-hover demo-table-dynamic table-responsive-block lang">
                  
                  <?php foreach ($csv_datas as $key => $csv_data):?>
                    <?php if ($key === 0): ?>
                    <thead>
                    <tr>
                    <?php foreach ($csv_data as $csv):?>
                    <th class="v-align-middle">
                      <?php echo $csv; ?>
                      </th>
                      <?php endforeach;?>
                      
                    </tr>
                  </thead>
                  <tbody>
                  <?php else: ?>
                    <tr>
                    <?php foreach ($csv_data as $csv):?>
                    <td class="v-align-middle">
                      <?php echo $csv; ?>
                      </td>
                      <?php endforeach;?>
                    </tr>
                    <?php endif; ?>

                    <?php endforeach;?>

                  </tbody>
                </table>
                <?php else: ?>
                         <p class="text-center"><?php echo $this->lang->line("file_exist"); ?></p>
                <?php endif; ?>
              </div>
            </div>
            <!-- END card -->
              </div>
              
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->
  
      </div>
      <!-- END PAGE CONTENT WRAPPER -->

      <?php endif; ?> 



       <!-- MODAL STICK UP SMALL ALERT -->
       <div class="modal fade slide-right" id="modalSlideLeft" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content-wrapper">
              
                <div class="modal-content">
                
                  <div class="container-xs-height full-height">
                  <a href="<?php echo base_url().'admin/dashboard';?>" aria-label="" class="pull-right close" type="link"><i class="pg-icon">close</i></a>
              
                  <?php if ($section === 'users'): ?>
                    <!-- START card -->
                <div class="card">
                  <div class="card-header ">
                    <div class="card-title">
                    <?php if(isset($edituser[0]['fname'])){echo $this->lang->line("update_user"); } else { echo $this->lang->line("create_user");  }; ?>
                    </div>
                  </div>
                  <div class="card-body">
                   <form action="<?php echo base_url();?>admin/crud" method="post" id="form-personal" role="form" autocomplete="off">
                      <div class="row clearfix">
                        <div class="col-xl-6">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("first_name"); ?></label>
                            <input type="text" class="form-control alphaonly" name="fname" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($edituser[0]['fname'])){echo $edituser[0]['fname']; }; ?>" >
                          </div>
                        </div>
                        <div class="col-xl-6">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("last_name"); ?></label>
                            <input type="text" class="form-control alphaonly" name="lname" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($edituser[0]['fname'])){echo $edituser[0]['lname']; }; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("organisation"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="oid" >                            
                            <?php foreach ($menus as $menu):?>
                              <option value="<?php echo $menu['id'];?>" <?php if(isset($edituser[0]['oid']) && $edituser[0]['oid'] == $menu['id']){echo 'selected'; } ?> ><?php echo $menu['name'];?></option>
                            <?php endforeach;?>
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("role"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="role" >                   
                            <option value="1" <?php if(isset($edituser[0]['role']) && $edituser[0]['role'] == 1){echo 'selected'; } ?> ><?php echo $this->lang->line("superadministrator"); ?></option>
                            <option value="2" <?php if(isset($edituser[0]['role']) && $edituser[0]['role'] == 2){echo 'selected'; } ?> ><?php echo $this->lang->line("administrator"); ?></option>
                            <option value="3" <?php if(isset($edituser[0]['role']) && $edituser[0]['role'] == 3){echo 'selected'; } ?> ><?php echo $this->lang->line("standarduser"); ?></option>
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("status"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="status" >                   
                            <option value="1" <?php if(isset($edituser[0]['status']) && $edituser[0]['status'] == 1){echo 'selected'; } ?> ><?php echo $this->lang->line("active"); ?></option>
                            <option value="0" <?php if(isset($edituser[0]['status']) && $edituser[0]['status'] == 0){echo 'selected'; } ?> ><?php echo $this->lang->line("disable"); ?></option>
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("restriction"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="locked" >                   
                        <option value="0" <?php if(isset($edituser[0]['locked']) && $edituser[0]['locked'] == 0){echo 'selected'; } ?> ><?php echo $this->lang->line("unlocked"); ?></option>                 
                        <option value="1" <?php if(isset($edituser[0]['locked']) && $edituser[0]['locked'] == 1){echo 'selected'; } ?> ><?php echo $this->lang->line("locked"); ?></option> 
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label><?php echo $this->lang->line("Password"); ?><?php echo $this->lang->line("password"); ?></label>
                            <input type="password" class="form-control" oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" name="password" placeholder="<?php echo $this->lang->line("password_length"); ?>" <?php if(isset($edituser[0]['password'])){echo 'value="'.$edituser[0]['password'].'"'; } else {echo 'required '; } ?> >
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label><?php echo $this->lang->line("email"); ?></label>
                            <input type="email" name="email" class="form-control" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" placeholder="<?php echo $this->lang->line("sample_email"); ?>" required value="<?php if(isset($edituser[0]['email'])){echo $edituser[0]['email']; }; ?>">
                          </div>
                        </div>
                      </div>
                      <input type="hidden" id="tablename" name="table" value="users" />
                      <div class="clearfix"></div>
                      <div class="row m-t-25">
                        <div class="col-xl-6 p-b-10">
                        <?php if (isset($edituser[0]['email'])): ?>
                        
                        <input type="hidden" name="id" value="<?php if(isset($edituser[0]['id'])){echo $edituser[0]['id']; }; ?>" />
                        <button aria-label="" class="btn btn-default m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("update_user"); ?></span></button>
                        <a href="<?php echo base_url().'admin/users/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?></span></a>
                        <?php else: ?>
                        <button aria-label="" class="btn btn-default  m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("create_user"); ?></span></button>
                        <a href="<?php echo base_url().'admin/users/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?></span></a>

                        <?php endif; ?>
                        </div>
                        <div class="col-xl-6">
                        
                       </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END card -->
                <script>
                    $('.alphaonly').bind('keyup blur',function(){ 
                       var node = $(this);
                        node.val(node.val().replace(/[^A-Za-z ]/g,'') ); });
                </script>

                <?php endif; ?> 

                <?php if ($section === 'organisations'): ?>
                  <!-- START card -->
                <div class="card">
                  <div class="card-header ">
                    <div class="card-title">
                    
                    <?php if(isset($editorganisation[0]['name'])){echo $this->lang->line("update_organisation"); } else { echo $this->lang->line("create_organisation");  }; ?>
                    </div>
                  </div>
                  <div class="card-body">
                   <form action="<?php echo base_url();?>admin/crud" method="post" id="form-personal" role="form" autocomplete="off">
                      <div class="row clearfix">
                        <div class="col-xl-6">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("organisation_name"); ?></label>
                            <input type="text" class="form-control" name="name" required value="<?php if(isset($editorganisation[0]['name'])){echo $editorganisation[0]['name']; }; ?>" >
                          </div>
                        </div>
                        <div class="col-xl-6">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("code"); ?></label>
                            <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("code_organisation_msg"); ?>" name="code" required value="<?php if(isset($editorganisation[0]['code'])){echo $editorganisation[0]['code']; }; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("organisation_status"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="status" >                   
                            <option value="1" <?php if(isset($editorganisation[0]['status']) && $editorganisation[0]['status'] == 1){echo 'selected'; } ?> ><?php echo $this->lang->line("active"); ?></option>
                            <option value="0" <?php if(isset($editorganisation[0]['status']) && $editorganisation[0]['status'] == 0){echo 'selected'; } ?> ><?php echo $this->lang->line("disable"); ?></option>
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("organisation_locked"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="locked" >                   
                        <option value="0" <?php if(isset($editorganisation[0]['locked']) && $editorganisation[0]['locked'] == 0){echo 'selected'; } ?> ><?php echo $this->lang->line("unlocked"); ?></option>                 
                        <option value="1" <?php if(isset($editorganisation[0]['locked']) && $editorganisation[0]['locked'] == 1){echo 'selected'; } ?>><?php echo $this->lang->line("locked"); ?></option> 
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label><?php echo $this->lang->line("email"); ?></label>
                            <input type="email" class="form-control" name="email" placeholder="org@gov.my" required value="<?php if(isset($editorganisation[0]['email'])){echo $editorganisation[0]['email']; }; ?>">
                          </div>
                        </div>
                      </div>
                      <input type="hidden" id="tablename" name="table" value="organisations" />
                      <div class="clearfix"></div>
                      <div class="row m-t-25">
                        <div class="col-xl-6 p-b-10">
                        <?php if (isset($editorganisation[0]['email'])): ?>
                        <input type="hidden" name="id" value="<?php if(isset($editorganisation[0]['id'])){echo $editorganisation[0]['id']; }; ?>" />
                        <button aria-label="" class="btn btn-default m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("update_organisation"); ?></span></button>
                        <a href="<?php echo base_url().'admin/organisations/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class="">Cancel</span></a>
                        <?php else: ?>
                        <input type="hidden" name="id" />
                        <button aria-label="" class="btn btn-default  m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("create_organisation"); ?></span></button>
                        <a href="<?php echo base_url().'admin/organisations/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class="">Cancel</span></a>

                        <?php endif; ?>
                        </div>
                        <div class="col-xl-6">
                        
                       </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END card -->
                <?php endif; ?> 

                <?php if ($section === 'site'): ?>
                   <!-- START card -->
                <div class="card">
                  <div class="card-header ">
                    <div class="card-title">
                    <?php if(isset($editsite[0]['name'])){echo $this->lang->line("update_site_config"); } else { echo $this->lang->line("create_site_config");  }; ?>
                    </div>
                  </div>
                  <div class="card-body">
                   <form action="<?php echo base_url();?>admin/crud" method="post" id="form-personal" role="form" autocomplete="off">
                      <div class="row clearfix">
                        <div class="col-md-12">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("config_name"); ?></label>
                            <input type="text" class="form-control" name="name" placeholder="<?php echo $this->lang->line("site_config_name"); ?>" required value="<?php if(isset($editsite[0]['name'])){echo $editsite[0]['name']; }; ?>" >
                          </div>
                        </div>
                        </div>
                        <div class="row clearfix">
                        <div class="col-md-12">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("config_value"); ?></label>
                            <input type="text" class="form-control" name="value" placeholder="<?php echo $this->lang->line("site_config_value"); ?>" required value="<?php if(isset($editsite[0]['value'])){echo $editsite[0]['value']; }; ?>">
                          </div>
                        </div>
                      </div>

                      <div class="row clearfix">
                        <div class="col-md-12">
                          <div class="form-group form-group-default required">
                            <label><?php echo $this->lang->line("config_description"); ?></label>
                            <input type="text" class="form-control" name="description" required value="<?php if(isset($editsite[0]['description'])){echo $editsite[0]['description']; }; ?>">
                          </div>
                        </div>
                      </div>
                      
                 
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("config_status"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="status" >                   
                            <option value="1" <?php if(isset($editsite[0]['status']) && $editsite[0]['status'] == 1){echo 'selected'; }; ?> ><?php echo $this->lang->line("active"); ?></option>
                            <option value="0" <?php if(isset($editsite[0]['status']) && $editsite[0]['status']== 0){echo 'selected'; }; ?> ><?php echo $this->lang->line("disable"); ?></option>
                        </select>
                      </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group ">
                        <label><?php echo $this->lang->line("config_locked"); ?></label>
                        <select class="full-width" data-init-plugin="select2" name="locked" >                   
                        <option value="0" <?php if(isset($editsite[0]['locked']) && $editsite[0]['locked'] == 0){echo 'selected'; } ?> ><?php echo $this->lang->line("unlocked"); ?></option>                 
                        <option value="1" <?php if(isset($editsite[0]['locked']) && $editsite[0]['locked'] == 1){echo 'selected'; } ?> ><?php echo $this->lang->line("locked"); ?></option> 
                        </select>
                      </div>
                        </div>
                      </div>
                      <input type="hidden" id="tablename" name="table" value="site" />
                      <div class="clearfix"></div>
                      <div class="row m-t-25">
                        <div class="col-xl-6 p-b-10">
                        <?php if (isset($editsite[0]['name'])): ?>
                        <input type="hidden" name="id" value="<?php if(isset($editsite[0]['id'])){echo $editsite[0]['id']; }; ?>" />
                        <button aria-label="" class="btn btn-default m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("update_site_config"); ?></span></button>
                        <a href="<?php echo base_url().'admin/site/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?></span></a>
                        <?php else: ?>
                        
                        <button aria-label="" class="btn btn-default  m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("create_site_config"); ?></span></button>
                        <a href="<?php echo base_url().'admin/site/';?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?></span></a>

                        <?php endif; ?>
                        </div>
                        <div class="col-xl-6">
                        
                       </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END card -->
                <?php endif; ?> 

                <?php if (($section === 'datasources') || ($section === 'dashboard')) : ?>
                  <!-- START card -->
                <div class="card">
                  <div class="card-header ">
                    <div class="card-title"><?php echo $this->lang->line("data_source"); ?>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                      <h3 class="mw-80">
                      <?php if(isset($editdatasource[0]['name'])){echo $this->lang->line("update_data_source"); } else { echo $this->lang->line("create_data_source"); }; ?>
                       </h3>'
                        
                        <br>
                        <form action="<?php echo base_url();?>admin/crud" method="post" id="form-work" class="form-horizontal" role="form" autocomplete="off">
                        <div class="form-group row">
                            <label class="col-md-5 control-label"><?php echo $this->lang->line("type"); ?></label>
                            <div class="col-md-7">
                            <div class="form-check-inline">
                            <label class="form-check-label">
                             <input type="radio" class="form-check-input" id="directapi" name="type" value="json" <?php if(empty($editdatasource[0]['type']) || $editdatasource[0]['type'] == 'json'){ echo 'checked'; } ?> ><?php echo $this->lang->line("direct_api"); ?>
                            </label>
                            </div>
                           <div class="form-check-inline">
                            <label class="form-check-label">
                            <input type="radio" class="form-check-input" id="sftpfile" name="type" value="csv" <?php if(isset($editdatasource[0]['type']) && $editdatasource[0]['type'] == 'csv'){ echo 'checked'; } ?> ><?php echo $this->lang->line("sftp_file"); ?>
                             </label>
                          </div>
                            </div>
                          </div>



                          <div class="form-group row">
                            <label for="ds" class="required col-md-5 control-label"><?php echo $this->lang->line("data_source_name"); ?></label>
                            <div class="col-md-7">
                              <input type="text" class="form-control" id="cdatasourcename" placeholder="<?php echo $this->lang->line("data_source_name"); ?>" name="name" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($editdatasource[0]['name'])){echo $editdatasource[0]['name']; } ?>">
                              <span class="help"><?php echo $this->lang->line("data_source_name_desc"); ?></span>
                            </div>
                          </div>

                            <div class="form-group row">
                            <label for="ds" class="required col-md-5 control-label"><?php echo $this->lang->line("data_source_description"); ?></label>
                            <div class="col-md-7">
                              <input type="text" class="form-control" id="cdatasourcename" placeholder="<?php echo $this->lang->line("data_source_description"); ?>" name="description" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($editdatasource[0]['description'])){echo $editdatasource[0]['description']; } ?>">
                              <span class="help"><?php echo $this->lang->line("data_source_name_desc"); ?></span>
                            </div>
                          </div>

                          <div class="form-group row" id="filename" <?php if(empty($editdatasource[0]['type']) || $editdatasource[0]['type'] == 'json'){ echo 'style="display:none"'; } ?> >
                            <label for="file" class="required col-md-5 control-label"><?php echo $this->lang->line("filename"); ?></label>
                            <div class="col-md-7">
                              <input type="text" class="form-control clearval" id="cdatasourcefilename" placeholder="<?php echo $this->lang->line("filename_sample"); ?>" name="file_name" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($editdatasource[0]['file_name'])){echo $editdatasource[0]['file_name']; } else { echo $this->lang->line("filename_sample"); } ?>">
                              <span class="help"><?php echo $this->lang->line("filename_desc"); ?> </span>
                            </div>
                          </div>

                              <div class="form-group row" id="searchfiles" <?php if(empty($editdatasource[0]['type']) || $editdatasource[0]['type'] == 'json'){ echo 'style="display:none"'; } ?>>
                            <label for="position" class="col-md-5 control-label" ></label>
                            <div class="col-md-7">
                              <button aria-label="" class="btn btn-default m-b-10 m-t-5" id="testdatasource1" type="button" data-target="#testsourcemodal" data-toggle="modal">Test Data Source Direct API</button>
                              <button aria-label="" style="display:none" class="btn btn-default m-b-10 m-t-5" id="testdatasource2" type="button" data-target="#testsourcemodal" data-toggle="modal"><?php echo $this->lang->line("search_data_files"); ?></button>
                              <p id="#datasourceresponse"></p>
                              <span class="help"><?php echo $this->lang->line("test_configuration_desc"); ?></span>
                            </div>
                          </div>

                          <div class="form-group row" id="apiurl" <?php if(isset($editdatasource[0]['type']) && $editdatasource[0]['type'] == 'csv'){ echo 'style="display:none"'; } ?>>
                            <label for="fname" class="required col-md-5 control-label"><?php echo $this->lang->line("apiurl"); ?></label>
                            <div class="col-md-7">
                              <input type="text" class="form-control clearval" id="url" placeholder="https://..." name="url" required oninvalid="this.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>')" oninput="setCustomValidity('')" value="<?php if(isset($editdatasource[0]['url'])){echo $editdatasource[0]['url']; } else { echo "https://example.com/api/1.."; } ?>">
                              <span class="help"><?php echo $this->lang->line("apiurl_desc"); ?></span>
                            </div>
                          </div>
                         
                          <div class="form-group row" id="header" <?php if(isset($editdatasource[0]['type']) && $editdatasource[0]['type'] == 'csv'){ echo 'style="display:none"'; } ?>>
                            <label for="name" class="col-md-5 control-label"><?php echo $this->lang->line("security_header"); ?></label>
                            <div class="col-md-7">
                            <select class="full-width" data-init-plugin="select2" id="header" name="header" >                   
                            <option value="application/json" >application/json</option>
                            <option value="application/json" >application/vnd.BNM.API.v1+json</option>
                           </select>
                             <span class="help"><?php echo $this->lang->line("security_header_desc"); ?></span>
                            </div>
                          </div>
                         

                        <?php  if($_SESSION['role'] == 1): ?>
                          <div class="form-group row" id="header">
                            <label for="name" class="col-md-5 control-label"><?php echo $this->lang->line("organisation"); ?></label>
                            <div class="col-md-7">
                        <select class="full-width" data-init-plugin="select2" name="oid" >                            
                            <?php foreach ($menus as $menu):?> 
                              <option value="<?php echo $menu['id']; ?>" <?php if(isset($editdatasource[0]['oid']) && $editdatasource[0]['oid'] == $menu['id']){ echo 'selected'; } ?> ><?php echo $menu['name'];?></option>
                            <?php endforeach;?>
                        </select>
                      </div>
                        </div>
                        <?php endif; ?> 

                        <div class="form-group row" id="clusters" <?php if(isset($editdatasource[0]['type']) && $editdatasource[0]['type'] == 'csv'){ echo 'style="display:none"'; } ?>>
                            <label for="name" class="col-md-5 control-label"><?php echo $this->lang->line("clusters"); ?></label>
                            <div class="col-md-7">
                            <select class="full-width" data-init-plugin="select2" id="clusters" name="clusters" >                   
                            <option value="kewangan">Kewangan</option>
                            <option value="bancian">Bancian</option>
                            <option value="jenayah">Jenayah</option>
                           </select>
                            
                            </div>
                          </div>

                              <div class="form-group row" id="header" <?php if(isset($editdatasource[0]['type']) && $editdatasource[0]['type'] == 'csv'){ echo 'style="display:none"'; } ?>>
                            <label for="name" class="col-md-5 control-label"><?php echo $this->lang->line("license"); ?></label>
                            <div class="col-md-7">
                            <select class="full-width" data-init-plugin="select2" id="header" name="header" >                   
                            <option value="cc-by" >Creative Commons Attribution</option>
                           
                           </select>
                            
                            </div>
                          </div>
                          
                      


                          <div class="form-group row">
                          <label for="position" class="col-md-7 control-label"><?php echo $this->lang->line("status_locked"); ?></label>
                          
                        <select class="col-md-6" data-init-plugin="select2" name="status" style="width: 90px">                   
                            <option value="1" <?php if(isset($editdatasource[0]['status']) && $editdatasource[0]['status'] == 1){ echo 'selected'; } ?> ><?php echo $this->lang->line("active"); ?></option>
                            <option value="0" <?php if(isset($editdatasource[0]['status']) && $editdatasource[0]['status'] == 0){ echo 'selected'; } ?> ><?php echo $this->lang->line("disable"); ?></option>
                        </select>
                        <select class="col-md-6" data-init-plugin="select2" name="locked" style="width: 130px">  
                            <option value="0" <?php if(isset($editdatasource[0]['locked']) && $editdatasource[0]['locked'] == 0){ echo 'selected'; } ?> ><?php echo $this->lang->line("unlocked"); ?></option>                 
                            <option value="1" <?php if(isset($editdatasource[0]['locked']) && $editdatasource[0]['locked'] == 1){ echo 'selected'; } ?> ><?php echo $this->lang->line("locked"); ?></option> 
                        </select>
                      
                      </div>
                    
                        
                   

                          <div class="form-group row">
                            <label for="position" class="col-md-5 control-label"><?php echo $this->lang->line("enable_automation"); ?></label>
                            <div class="col-md-7">
                              
                              <input type="checkbox" data-init-plugin="switchery" data-size="small" data-color="primary" name="automation" <?php if(isset($editdatasource[0]['automation'])){ echo 'checked="checked"'; } ?> />
                              <span class="help"><?php echo $this->lang->line("automation_desc"); ?></span>
                            </div>
                          </div>

                    
                          <br>
                          <div class="row">
                            <div class="col-md-6">
                              <p class="small-text hint-text"></p>
                            </div>
                            <div class="col-md-6">
                              <input type="hidden" id="tablename" name="table" value="datasources" />
                              
                              
                        <?php if (isset($editdatasource[0]['name'])): ?>
                        <input type="hidden" name="id" value="<?php if(isset($editdatasource[0]['id'])){echo $editdatasource[0]['id']; }; ?>" />
                        <button aria-label="" class="btn btn-default m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("update_data_source"); ?></span></button>
                        <a href="<?php echo base_url().'admin/datasources/'.$editdatasource[0]['oid'];?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?><<?php echo $this->lang->line("cancel"); ?></span></a>
                        <?php else: ?>
                        <button aria-label="" class="btn btn-default  m-b-10"  type="submit"><span class=""><?php echo $this->lang->line("create_data_source"); ?></span></button>
                        <a href="<?php echo base_url().'admin/datasources/'.$_SESSION["oid"];?>" aria-label="" class="btn btn-default  m-b-10" type="button"><span class=""><?php echo $this->lang->line("cancel"); ?></span></a>

                        <?php endif; ?>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END card -->
                <?php endif; ?> 

             
                 
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP SMALL ALERT -->


<script>
$("#sftpfile").click(function(){
  $("#apiurl").hide();
  $("#header").hide();
  $("#filename").show();
  $("#searchfiles").show();
  $("#filename").val("-");
  $("#testdatasource2").show();
  $("#testdatasource1").hide();
});
</script>

<script>
$("#directapi").click(function(){
  $("#apiurl").show();
  $("#header").show();
  $("#filename").hide();
  $("#searchfiles").hide();
  $("#testdatasource2").hide();
  $("#testdatasource1").show();
});
</script>
<script>
$( ".clearval" ).click(function() {
  $(this).val('');
});
</script>

<?php if (!empty($edit)): ?>
          <script>
    $(document).ready(function(){
        $("#modalSlideLeft").modal('show');
    });
</script>
<?php endif; ?>

           <!-- MODAL STICK UP SMALL ALERT -->
           <div class="modal fade stick-up" id="modalStickUpSmall" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon"><?php echo $this->lang->line("close"); ?></i>
                    </button>
                    <h5>Success !</h5>
                  </div>
                  <div class="modal-body">
                    <p class="no-margin"> <?php echo $this->session->flashdata('msg'); ?></p>
                  </div>
                  <div class="modal-footer">
                    <button aria-label="" type="button" class="btn btn-default no-margin pull-left inline" data-dismiss="modal"><?php echo $this->lang->line("close"); ?></button>
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP SMALL ALERT -->

  <?php if (!empty($alert)): ?>
          <script>
    $(document).ready(function(){
        $("#modalStickUpSmall").modal('show');
    });
</script>

<?php endif; ?> 

          <script>
          $("#testdatasource1").click(function(){
          var url = $("#url").val();
          var header = $("#header").val();
          var settings = {
            'cache': false,
          'dataType': "jsonp",
          "async": true,
            "crossDomain": true,
            "url": url,
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Accept": header,
            "Access-Control-Request-Headers": "x-requested-with",
            "Access-Control-Allow-Origin": "*"
          },
        };

        $.ajax(settings).done(function (response) {
          console.log(response);
          var returnedData = JSON.stringify(response);        
        $("#datasourceresponse").html("<b class='text-green'>SUCCESS</b><br>"+returnedData);
        });
      });
      </script>

<script>
$("#testdatasource2").click(function() {
var url = $("#cdatasourcefilename").val().trim();
if(url == 'jumlah-pengguna-kenderaan-awam-2019.csv'){
$("#datasourceresponse").html('<b class="text-green">SUCCESS</b><br> TAHUN,MOTOSIKAL,MOTOKAR,BAS,TEKSI,KENDERAAN BARANG,LAIN-LAIN KENDERAAN 2011,18,732,13,492,24,63,118,237 2012,19,568,14,476,22,30,1,087,2632013,12,876,10,924,13,15,1,332,1662014,18,869,10,127,51,11,1,988,2142015,16,902,7,669,23,0,1,788,1612016,15,090,2,622,13,28,1,551,1682017,11,416,1,787,9,4,2,111,1822018,10,143,1,723,9,1,2,376,217');
$("#cdatasourcename").prop("disabled", true);
}
         });

 </script>


<!-- MODAL STICK UP SMALL ALERT -->
<div class="modal fade slide-right" id="testsourcemodal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix">
                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                    </button>
                    <h5>Senarai Data SFTP</h5>
                  </div>
                  

                    <table id="example" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Kementerian</th>
                <th>Nama</th>
                <th>Tarikh</th>
               
              
            </tr>
        </thead>
        <tbody>
<?php         
   $path = getcwd().'/raw_data/';
   $cdir = scandir($path);
   foreach ($cdir as $key => $value)
   {
      if (!in_array($value,array(".","..")))
      {
        if(is_dir($path.$value)){
        
        $cdir1 = scandir($path.$value);
        foreach ($cdir1 as $file){
             $mod_date = date("d-M-Y H:i:s", filemtime($path.$value.'/'.$file));
             if (strpos($file,'.csv') !== false){
             echo '<tr><td>'.$value.'</td><td>'.str_replace('.csv','',$file).'</td><td>'.$mod_date.'</td></tr>';
             }
            
        }
        
        }
      }
   }
   ?>
            </tbody>
            </table>
            <style>
            div.dataTables_info { 
              padding:0px !important;
              white-space: normal !important;
                }
            </style>
            <script>
            $(document).ready(function() {
    $('#example').DataTable();
} );
</script>

                 
                  <div class="modal-footer">
                    <button aria-label="" type="button" class="btn btn-primary pull-left inline" data-dismiss="modal">Tutup</button>
                  
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP SMALL ALERT -->


          <!-- MODAL STICK UP DELETE ALERT -->
<div class="modal fade stick-up" id="delete" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content-wrapper">
                <div class="modal-content">
                  <div class="modal-header clearfix text-left">
                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                    </button>
                    <h5><?php echo $this->lang->line("delete"); ?></h5>
                  </div>
                  <div class="modal-body">
                  <div id="deletemsg">delete info</div>
                  </div>
                  <div class="modal-footer">
                    <a href="#" id="deletelink" aria-label="" type="button" class="btn btn-primary m-b-10"><?php echo $this->lang->line("proceed"); ?></a>
                    <button aria-label="" type="button" class="btn btn-primary m-b-10" data-dismiss="modal"><?php echo $this->lang->line("cancel"); ?></button>
                  
                  </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- END MODAL STICK UP DELETE ALERT -->
          <script>
          $(".delete").on('click', function(event){
            var del = this.id;
            var ar = del.split('-')
            $("#deletemsg").text("ID:"+ar[0]+" "+ar[3]+" ?");
            $("#deletelink").attr("href", "<?= base_url();?>admin/delete/"+ar[1]+"/"+ar[0]+"/"+ar[2]);
            });

          </script>

<script>
 $(document).ready( function() {
var table = $('.lang');

var settings = {
    "sDom": "<t><'row'<p i>>",
    "destroy": true,
    "scrollCollapse": true,
    "language": {
        "sLengthMenu": "_MENU_ ",
        "search": "Carian:",
        "infoFiltered": "<?php echo $this->lang->line("datatables_filter"); ?>",
        "infoEmpty": "<?php echo $this->lang->line("datatables_infoempty"); ?>",
        "zeroRecords": "<?php echo $this->lang->line("datatables_zero"); ?>",
        "emptyTable": "<?php echo $this->lang->line("datatables_empty"); ?>",
        "sInfo": "<?php echo $this->lang->line("datatables_entries"); ?>"
    },
    "iDisplayLength": 5
};

table.dataTable(settings);

// search box for table
     $('#search-table').keyup(function() {
    table.fnFilter($(this).val());
      });
} );
</script>

<script>
function InvalidMsg(textbox) {
    if (textbox.value === '') {
        textbox.setCustomValidity('<?php echo $this->lang->line("required_field"); ?>');
    } else if (textbox.validity.typeMismatch){
        textbox.setCustomValidity('<?php echo $this->lang->line("invalid_email"); ?>');
    } else {
       textbox.setCustomValidity('');
    }

    return true;
}
</script>

          
         

  


 



    

   