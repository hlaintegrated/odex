<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $this->lang->line("page_title"); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="<?= base_url();?>assets/ico/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
    <meta content="Ace" name="author" />
    <link href="<?= base_url();?>assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?= base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?= base_url();?>assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/css/dashboard.widgets.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url();?>assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="<?= base_url();?>assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
    <link href="<?= base_url();?>assets/plugins/ion-slider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/ion-slider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?= base_url();?>assets/plugins/jquery-nouislider/jquery.nouislider.css" rel="stylesheet" type="text/css" media="screen" />
    <link class="main-stylesheet" href="<?= base_url();?>assets/css/themes/light.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url();?>assets/js/slider.js" type="text/javascript"></script>
    <style>
    .form-horizontal .form-group .control-label.required:after {
         content: "<?php echo $this->lang->line("required"); ?>";
         position: absolute;
         right: 0px;
         font-size: 12px;
         margin-right: 4px;
        color: rgba(101, 108, 119, 0.87);
      }
   </style>
  </head>
  <body class="fixed-header dashboard menu-pin">
    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
      <img src="<?php echo base_url();?>assets/img/dtsalogo.png" alt="logo" class="img-fluid text-center brand" data-src="<?php echo base_url();?>assets/img/dtsalogo.png" data-src-retina="<?php echo base_url();?>assets/img/dtsalogo.png" width="170px">
      </div>
      <div class="text-center mute"><small><?php echo $this->lang->line("odex"); ?></small></div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <li class="m-t-10">
            <a href="<?php echo base_url();?>admin/dashboard" class="detailed">
              <span class="title"><?php echo $this->lang->line("dashboard"); ?></span>
            </a>
            <span class="icon-thumbnail"><i data-feather="shield"></i></span>
          </li>

          <li>
            <a href="javascript:;"><span class="title">API</span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i data-feather="calendar"></i></span>
            <ul class="sub-menu">
            <?php foreach ($menus as $menu):?>
              
              <li class="">
                <a href="<?= base_url();?>admin/datasources/<?php echo $menu['id'].'/';?>"><?php echo $menu['name'];?></a>
                <span class="icon-thumbnail">
                <i class="pg-icon success">circle_fill</i>
                </span>
              </li>

            <?php endforeach;?>

            </ul>
          </li>
          <li>
            <a href="javascript:;"><span class="title"><?php echo $this->lang->line("settings"); ?></span>
            <span class=" arrow"></span></a>
            <span class="icon-thumbnail"><i data-feather="calendar"></i></span>
            <ul class="sub-menu">
              <li class="">
                <a href="<?= base_url();?>admin/users"><?php echo $this->lang->line("users"); ?></a>
                <span class="icon-thumbnail">
                <i class="pg-icon success">users</i>
                </span>
              </li>
              <li class="">
                <a href="<?= base_url();?>admin/organisations"><?php echo $this->lang->line("organisations"); ?></a>
                <span class="icon-thumbnail">
                <i data-feather="briefcase"></i>
                </span>
              </li>
              <li class="">
                <a href="<?= base_url();?>admin/site"><?php echo $this->lang->line("site"); ?></a>
                <span class="icon-thumbnail">
                <i data-feather="layout"></i>
                </span>
              </li>
             
           
            </ul>
          </li>
          <li class="">
            <a href="<?= base_url();?>admin/logs" ><span class="title"><?php echo $this->lang->line("logs"); ?></span></a>
            <span class="icon-thumbnail"> <i data-feather="list"></i></span>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header bg-primary">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar d-lg-none  pg-icon btn-icon-link" data-toggle="sidebar">
      		menu</a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
          <div class="brand inline">
            <img src="<?php echo base_url();?>assets/img/dtsalogo_white.png" alt="logo" data-src="<?php echo base_url();?>assets/img/dtsalogo_white.png" data-src-retina="<?php echo base_url();?>assets/img/dtsalogo_white.png" width="150">
          </div>
       </div>
        <div class="d-flex align-items-center">
          
          <!-- START User Info-->
          <div class="pull-left p-r-10 fs-14 d-lg-inline-block d-none m-l-20">
            <span class="semi-bold"><?php if(empty($_SESSION['email'])){ redirect('user/logout'); } else { echo $_SESSION['email'];  }; ?></span>
            <a href="<?php echo base_url();?>user/logout" class="btn btn-white btn-xs m-b-3" type="link"><i class="pg-icon">exit</i></a>
          </div>
      
        </div>
      </div>
      <!-- END HEADER -->
      
      <!-- START PAGE CONTENT WRAPPER -->
      <?= $contents ?>
      
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
   
    
    <!-- BEGIN VENDOR JS -->
    <script src="<?= base_url();?>assets/plugins/feather-icons/feather.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <!--  A polyfill for browsers that don't support ligatures: remove liga.js if not needed-->
    <script src="<?= base_url();?>assets/plugins/liga.js" type="text/javascript"></script>

    <script src="<?= base_url();?>assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/classie/classie.js"></script>
    <script src="<?= base_url();?>assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/utils.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/rickshaw/rickshaw.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/mapplic/js/hammer.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/mapplic/js/jquery.mousewheel.js"></script>
    <script src="<?= base_url();?>assets/plugins/mapplic/js/mapplic.js"></script>
    <script src="<?= base_url();?>assets/js/dashboard.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>	
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>	
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>	
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>	
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>	
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
 

    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <?php if ($_SESSION["language"] == "malay") : ?>
    <script src="<?= base_url();?>assets/js/datatables_my.js" type="text/javascript"></script>
    <?php endif; ?>
    <?php if ($_SESSION["language"] == "english") : ?>
    <script src="<?= base_url();?>assets/js/datatables_en.js" type="text/javascript"></script>
    <?php endif; ?> 

    <script src="<?= base_url();?>assets/js/notifications.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/js/tables.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/js/dashboard.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/js/scripts.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/ion-slider/js/ion.rangeSlider.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-nouislider/jquery.nouislider.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>assets/plugins/jquery-nouislider/jquery.liblink.js" type="text/javascript"></script>

       <!-- BEGIN CORE TEMPLATE JS -->
       <script src="<?= base_url();?>assets/js/pages.js"></script>
    <!-- END CORE TEMPLATE JS -->
        <!-- BEGIN PAGE LEVEL JS -->
        <script src="<?= base_url();?>assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
  
    <!-- END PAGE LEVEL JS -->



  </body>
</html>


