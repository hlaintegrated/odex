<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- START CONTAINER FLUID -->
          <div class=" container-fluid   container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users Management</li>
            </ol>
            <!-- END BREADCRUMB -->
            <div class="row">
              <div class="col-xl-6 col-lg-6 ">
             <!-- START card -->
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover demo-table-dynamic table-responsive-block" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th>Email</th>
                      <th>Organisation</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="v-align-middle">
                        test@gmail.com
                      </td>
                      <td class="v-align-middle">
                        <p>Bank Negara</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Active</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Notes go here</p>
                      </td>
                    </tr>
                    <tr>
                      <td class="v-align-middle">
                        test@gmail.com
                      </td>
                      <td class="v-align-middle">
                        <p>Bank Negara</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Active</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Notes go here</p>
                      </td>
                    </tr>
                    <tr>
                      <td class="v-align-middle">
                        test@gmail.com
                      </td>
                      <td class="v-align-middle">
                        <p>Bank Negara</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Active</p>
                      </td>
                      <td class="v-align-middle">
                        <p>Notes go here</p>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END card -->
              </div>
              <div class="col-xl-6 col-lg-6 ">
                <!-- START card -->
                <div class="card">
                  <div class="card-header ">
                    <div class="card-title">Update User
                    </div>
                  </div>
                  <div class="card-body">
                   <form id="form-personal" role="form" autocomplete="off">
                      <div class="row clearfix">
                        <div class="col-xl-6">
                          <div class="form-group form-group-default required">
                            <label>First name</label>
                            <input type="text" class="form-control" name="firstName" required>
                          </div>
                        </div>
                        <div class="col-xl-6">
                          <div class="form-group form-group-default">
                            <label>Last name</label>
                            <input type="text" class="form-control" name="lastName" required>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                              <label>Pages username</label>
                              <input type="text" class="form-control" name="website" placeholder="http://pages-ui.com/projectname" required>
                            </div>
                            <div class="input-group-append ">
                              <span class="input-group-text">@pages.com
										</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Minimum of 4 characters." required>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="example@address.com" required>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="row m-t-25">
                        <div class="col-xl-6 p-b-10">
                          <p class="small-text hint-text">By clicking the "Get Started!" button, you are creating a Pages account, and you agree to Pages's <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                        </div>
                        <div class="col-xl-6">
                          <button aria-label="" class="btn btn-primary pull-right btn-lg btn-block" type="submit">Get Started
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- END card -->
              </div>
            </div>
          </div>
          <!-- END CONTAINER FLUID -->
          
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid  container-fixed-lg footer">
          <div class="copyright sm-text-center">
       
            <p class="small no-margin pull-right sm-pull-reset">
             <span class="hint-text">©2020 All Rights Reserved MAMPU</span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->