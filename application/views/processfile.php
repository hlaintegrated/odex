<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content sm-gutter">
          <!-- START JUMBOTRON -->
          <div data-pages="parallax">
            <div class="container-fluid p-l-25 p-r-25 sm-p-l-0 sm-p-r-0">
              <div class="inner">
                <!-- START BREADCRUMB -->
                <ol class="breadcrumb sm-p-b-5">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Dashboard</li>
                </ol>
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid p-l-25 p-r-25 p-t-0 p-b-25 sm-padding-10">
            <!-- START ROW -->
            <b># Copy CSV file from Agency Server</b> <i class="fas fa-check-circle"></i><br>
            <b># Get File Data</b> <i class="fas fa-check-circle"></i><br>
            <?php echo $getjson; ?>
           
            <br><b># Check Data exist (New Insert or Update)</b> <i class="fas fa-check-circle"></i><br>

            <br><b># Data Cleaning and Validity </b> <i class="fas fa-check-circle"></i><br>


            <br><b># Data Mapping </b> <i class="fas fa-check-circle"></i><br>

            <br><b># CSV to JSON</b> <i class="fas fa-check-circle"></i><br>

            <br><b># Retrived Package ID </b><br><div style="color:green"> <?php echo $packageid; ?></div>

            <br><b># CKAN Resource Creation </b> <i class="fas fa-check-circle"></i><br>
              
            <?php echo $ckancreateresource; ?>

            <br><b># CKAN Datasource Creation </b> <i class="fas fa-times-circle"></i><br>

            <?php echo $ckandatastorecreate; ?>

            <br><b># CKAN Data Source Update / Create </b><br>

            <br><b># Send Notification to Admin / User </b><br>

           <br><br>
          </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
       
      
      </div>